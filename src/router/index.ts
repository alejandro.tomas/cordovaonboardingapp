import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Paso1",
    component: () =>
      import(/* webpackChunkName: "Paso1" */ "../views/paso1/index.vue"),
  },
  {
    path: "/paso2",
    name: "Paso2",
    component: () =>
      import(/* webpackChunkName: "Paso2" */ "../views/paso2/index.vue"),
  },
  {
    path: "/paso3",
    name: "Paso3",
    component: () =>
      import(/* webpackChunkName: "Paso3" */ "../views/paso3/index.vue"),
  },
  {
    path: "/paso4",
    name: "Paso4",
    component: () =>
      import(/* webpackChunkName: "Paso4" */ "../views/paso4/index.vue"),
  },
  {
    path: "/paso5",
    name: "Paso5",
    component: () =>
      import(/* webpackChunkName: "Paso5" */ "../views/paso5/index.vue"),
  },
  {
    path: "/digital-error",
    name: "DigitalError",
    component: () =>
      import(
        /* webpackChunkName: "DigitalError" */ "../views/digital-error/index.vue"
      ),
  },
  {
    path: "/paso6",
    name: "Paso6",
    component: () =>
      import(/* webpackChunkName: "Paso6" */ "../views/paso6/index.vue"),
  },
  {
    path: "/paso7",
    name: "Paso7",
    component: () =>
      import(/* webpackChunkName: "Paso7" */ "../views/paso7/index.vue"),
  },
  {
    path: "/paso8",
    name: "Paso8",
    component: () =>
      import(/* webpackChunkName: "Paso8" */ "../views/paso8/index.vue"),
  },
  {
    path: "/paso9",
    name: "Paso9",
    component: () =>
      import(/* webpackChunkName: "Paso9" */ "../views/paso9/index.vue"),
  },
  {
    path: "/paso10",
    name: "Paso10",
    component: () =>
      import(/* webpackChunkName: "Paso10" */ "../views/paso10/index.vue"),
  },
  {
    path: "/paso11",
    name: "Paso11",
    component: () =>
      import(/* webpackChunkName: "Paso11" */ "../views/paso11/index.vue"),
  },
  {
    path: "/paso12",
    name: "Paso12",
    component: () =>
      import(/* webpackChunkName: "Paso12" */ "../views/paso12/index.vue"),
  },
  {
    path: "/paso13",
    name: "Paso13",
    component: () =>
      import(/* webpackChunkName: "Paso13" */ "../views/paso13/index.vue"),
  },
  {
    path: "/paso14",
    name: "Paso14",
    component: () =>
      import(/* webpackChunkName: "Paso14" */ "../views/paso14/index.vue"),
  },
  {
    path: "/paso15",
    name: "Paso15",
    component: () =>
      import(/* webpackChunkName: "Paso15" */ "../views/paso15/index.vue"),
  },
  {
    path: "/paso16",
    name: "Paso16",
    component: () =>
      import(/* webpackChunkName: "Paso16" */ "../views/paso16/index.vue"),
  },
  {
    path: "/paso17",
    name: "Paso17",
    component: () =>
      import(/* webpackChunkName: "Paso17" */ "../views/paso17/index.vue"),
  },
  {
    path: "/paso18",
    name: "Paso18",
    component: () =>
      import(/* webpackChunkName: "Paso18" */ "../views/paso18/index.vue"),
  },

  // catch all redirect
  { path: "*", redirect: "/" },
];

const router = new VueRouter({
  // mode: "history",
  base: process.env.BASE_URL,
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
  routes,
});

export default router;
