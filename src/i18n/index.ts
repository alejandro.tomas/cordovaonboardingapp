import en from './en';
import es from './es';
import Vue from 'vue';
import VueI18n from 'vue-i18n';

Vue.use(VueI18n);

const i18n = new VueI18n({
    locale: 'es-ES',
    messages: {
        'es-ES': es,
        'en-US': en
    }
});

export default i18n;
