export default {
  CASADO: 'Casado',
  DIVORCIADO: 'Divorciado',
  PAREJA_DE_HECHO: 'Pareja de hecho',
  SEPARADO_DE_HECHO: 'Separado de hecho',
  SEPARADO_JUDICIAL: 'Separado judicial',
  SOLTERO: 'Soltero',
  VIUDO: 'Viudo'
};
