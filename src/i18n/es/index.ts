import countries from './countries.es';
import streetTypes from './street-types.es';
import spanishProvinces from './spanish-provinces';
import kyc from './kyc.es';
import maritalStatuses from './marital-statuses';
import intervenersStats from './interveners.es';
import convenience from './convenience.es';
import operations from './operations.es';
import documents from './documents.es';

export default {
  countries,
  streetTypes,
  spanishProvinces,
  kyc,
  maritalStatuses,
  intervenersStats,
  convenience,
  operations,
  documents
};
