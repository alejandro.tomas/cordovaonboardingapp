export default {
    products: {
        PP: {
            COBAS_GLOBAL: 'Cobas Global',
            COBAS_MIXTO_GLOBAL: 'Cobas Mixto Global'
        },
        FI: {
            COBAS_SELECCION: 'Cobas Selección',
            COBAS_INTERNACIONAL: 'Cobas Internacional',
            COBAS_IBERIA: 'Cobas Iberia',
            COBAS_GRANDES_COMPANIAS: 'Cobas Grandes Compañías',
            COBAS_RENTA: 'Cobas Renta',
            COBAS_CONCENTRADOS_FIL: 'Cobas Concentrados'
        }
    },
    operations_types: {
        PP: {
            SOLICITUD_APORTACION_PERIODICA: 'Aportación Periódica',
            APORTACION: 'Aportación',
            TRASPASO_EXTERNO: 'Traspaso Externo'
        },
        FI: {
            SUSCRIPCION: 'Suscripción',
            SOLICITUD_SUSCRIPCION_PERIODICA: 'Suscripción Periódica',
            TRASPASO_EXTERNO: 'Traspaso Externo'
        }
    },
    sources: {
        ONLINE: 'Online',
        PRESENCIAL: 'Presencial',
        CORREO: 'Correo',
        MIGRACION: 'Migracion'
    },
    pay_methods: {
        DOMICILIACION: 'Domiciliacion',
        TRANSFERENCIA: 'Transferencia',
        CHEQUE: 'Cheque',
        OMF: 'OMF'
    },
    rights_types: {
        C: 'Derechos consolidados',
        E: 'Derechos económicos',
        A: 'Ambos'
    },
    source_situations: {
        PARTICIPE: 'Participe - derechos consolidados',
        BENEFICIARIO: 'Beneficiario - derechos económicos'
    },
    compartments: {
        1: 'Anteriores a 2007',
        2: 'Posteriores a 2007',
        3: 'De ambos'
    },
    retirement_plan_type: {
        PPA: "Plan de previsión asegurado (PPA)",
        PP: "Plan de pensiones (PP)"
    },
    indicators: {
        B: 'Importe',
        P: 'Participaciones'
    },
    periodicity_options: {
        MENSUAL: 'Mensual',
        TRIMESTRAL: 'Trimestral',
        SEMESTRAL: 'Semestral',
        ANUAL: 'Anual'
    },
    revaluation_options: {
        'no-increase': 'Sin incremento',
        LINEAL: 'Lineal',
        PORCENTUAL: 'Porcentual',
        ACUMULATIVO: 'Acumulativo'
    }
};
