/* eslint-disable  @typescript-eslint/no-explicit-any */
import { MutationTree } from 'vuex';

const mutations: MutationTree<any> = {
  SET_LEAD_UUID(state, lead_uuid) {
    state.lead_uuid = lead_uuid;
  },
  SET_AGENT(state, agent) {
    state.agent = agent;
  },
  SET_DATA(state, payload) {
    state[payload.step] = payload.data;
  },
  SET_EDITING(state, isEditing) {
    state.isEditing = isEditing;
  },
  SET_ADDING_SECONDARY_INTERVENER(state, isAddingSecondaryIntervener) {
    state.isAddingSecondaryIntervener = isAddingSecondaryIntervener;
  },
  SET_OPERATIONS_TYPE(state, operationsType) {
    state.operationsType = operationsType;
  },
  SET_INTERVENER(state, intervener) {
    state.interveners.push(intervener);
  },
  SET_INTERVENER_ID(state, intervener) {
    const intervenerId = intervener.id;
    delete intervener.id;
    state.interveners[intervenerId] = intervener;
  },
  SET_OPERATION(state, operation) {
    state.operations.push(operation);
  },
  SET_OPERATION_ID(state, operation) {
    const operationId = operation.id;
    delete operation.id;
    state.operations[operationId] = operation;
  },

  SET_ROLE(state, payload) {
    state.interveners[payload.index].role = payload.role;
  },
  REMOVE_INTERVENER(state, index) {
    state.interveners.splice(index, 1);
  },
  REMOVE_OPERATION(state, index) {
    state.operations.splice(index, 1);
  },
  SET_TYPE_PERSON(state, payload) {
    state.isJuridicalEntity = payload.isJuridicalEntity;
    state.settedIsJuridicalEntity = payload.settedIsJuridicalEntity;
    state.createInterveners = payload.createInterveners;
  },
  SET_INTERVENER_ADDRESS(state, payload) {
    state.interveners[payload.idIntervener].addresses = payload.addresses;
  },
  LOAD_DATA_TO_STATE(state, payload) {
    state.account = payload.account;
    state.agent = payload.agent;
    state.createInterveners = payload.createInterveners || false;
    state.interveners = payload.interveners;
    state.isAddingSecondaryIntervener = payload.isAddingSecondaryIntervener;
    state.isEditing = payload.isEditing;
    state.isJuridicalEntity = payload.isJuridicalEntity;
    state.lead_uuid = payload.lead_uuid;
    state.operations = payload.operations || [];
    state.operationsCreated = payload.operationsCreated || [];
    state.operationsType = payload.operationsType;
    state.settedIsJuridicalEntity = payload.settedIsJuridicalEntity;
    state.step2 = payload.step2;
    state.step3 = payload.step3;
    state.step4 = payload.step4;
    state.step5 = payload.step5;
    state.step6 = payload.step6;
    state.step7 = payload.step7;
    state.step8 = payload.step8;
    state.step9 = payload.step9;
    state.step10 = payload.step10;
    state.step11 = payload.step11;
    state.step12 = payload.step12;
    state.step13 = payload.step13;
    state.step14 = payload.step14;
    state.operations = payload.operations || [];
  }
};
export default mutations;
