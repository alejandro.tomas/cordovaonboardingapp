/* eslint-disable  @typescript-eslint/no-explicit-any */
import { ActionTree } from 'vuex';
import { ApplicationState } from '@/store/application/state';

const actions: ActionTree<ApplicationState, any> = {
  setLeadUuid: ({ commit }, lead_uuid: any) => {
    commit('SET_LEAD_UUID', lead_uuid);
  },
  setAgent: ({ commit }, agent: any) => {
    commit('SET_AGENT', agent);
  },
  setDataStep: ({ commit }, payload: any) => {
    commit('SET_DATA', payload);
  },
  setEditing: ({ commit }, payload: any) => {
    commit('SET_EDITING', payload);
  },
  setAddingSecondaryIntervener: ({ commit }, payload: any) => {
    commit('SET_ADDING_SECONDARY_INTERVENER', payload);
  },
  setOperationsType: ({ commit }, payload: any) => {
    commit('SET_OPERATIONS_TYPE', payload);
  },
  setIntervener: ({ commit }, payload: any) => {
    commit('SET_INTERVENER', payload);
  },
  setIntervenerById: ({ commit }, payload: any) => {
    commit('SET_INTERVENER_ID', payload);
  },
  setOperation: ({ commit }, payload: any) => {
    commit('SET_OPERATION', payload);
  },
  setOperationById: ({ commit }, payload: any) => {
    commit('SET_OPERATION_ID', payload);
  },
  setRole: ({ commit }, payload: any) => {
    commit('SET_ROLE', payload);
  },
  removeIntervener: ({ commit }, index: number) => {
    commit('REMOVE_INTERVENER', index);
  },
  removeOperation: ({ commit }, index: number) => {
    commit('REMOVE_OPERATION', index);
  },
  setIsJuridicalEntity: ({ commit }, payload: any) => {
    commit('SET_TYPE_PERSON', payload);
  },
  setAddressToIntervener: ({ commit }, payload: any) => {
    commit('SET_INTERVENER_ADDRESS', payload);
  },
  loadToState: ({ commit }, payload: any) => {
    commit('LOAD_DATA_TO_STATE', payload);
  }
};
export default actions;
