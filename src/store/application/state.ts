/* eslint-disable  @typescript-eslint/no-explicit-any */
export interface ApplicationState {
    lead_uuid?: string;
    isJuridicalEntity?: boolean;
    isEditing?: boolean;
    isAddingSecondaryIntervener?: boolean;
    operationsType: string;
    settedIsJuridicalEntity?: boolean;
    createInterveners?: boolean;
    agent?: any;
    account?: any;
    interveners: any[];
    operations: any[];
    step2?: any;
    step3?: any;
    step4?: any;
    step8?: any;
    step10?: any;
    step12?: any;
}
const state: ApplicationState = {
    lead_uuid: '',
    isJuridicalEntity: false,
    isEditing: false,
    isAddingSecondaryIntervener: false,
    operationsType: '',
    settedIsJuridicalEntity: false,
    createInterveners: false,
    agent: '',
    account: {},
    interveners: [],
    operations: [],
    step2: {},
    step3: {},
    step4: {},
    step8: [],
    step10: {},
    step12: {}
};
export default state;
