/* eslint-disable no-shadow */
/* eslint-disable  @typescript-eslint/no-explicit-any */
import { GetterTree } from 'vuex';
import { ApplicationState } from './state';

const getters: GetterTree<ApplicationState, any> = {

    leadUuid(state) {
        return state.lead_uuid;
    },

    applicationData(state) {
        return state;
    }
};
export default getters;
