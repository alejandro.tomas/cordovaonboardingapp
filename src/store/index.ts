import Vue from "vue";
import Vuex from "vuex";
import applicationStore from './application/index';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        applicationStore,
    }

//   state: {},
//   mutations: {},
//   actions: {},
//   modules: {}
});
