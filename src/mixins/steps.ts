import Vue from 'vue';
import Component from 'vue-class-component';

@Component
export default class StepsMixin extends Vue {
  updateOnboardingState(leadUuid: string, actual_step: number, nextStep?: any) {
    if (!leadUuid) {
      const { lead_uuid } = this.$store.getters['applicationStore/applicationData'];
      leadUuid = lead_uuid;
    }

    const payload = {
      state_id: leadUuid,
      actual_step: actual_step,
      state_json: this.$store.getters['applicationStore/applicationData']
    };
    this.$onboardingService
      .updateOnboardingState(payload)
      .then(() => {
        if (nextStep && `/${nextStep}` !== this.$route.path) {
          this.$router.push(`/${nextStep}`);
        } else if (`/paso${actual_step + 1}` !== this.$route.path) {
          this.$router.push(`/paso${actual_step + 1}`);
        }
      })
      .catch((err: Error) => {
        console.error(err);
        this.$bvToast.toast(`No se ha podido actualizar el estado de lead.`, {
          title: `Error al actualizar paso`,
          autoHideDelay: 5000,
          variant: 'danger'
        });
      });
  }
}
