import Vue from 'vue'
import Component from 'vue-class-component'
import spainIdType from '@/utils/checkSpanishIdType';
import validateCIF from '@/utils/validateCif';
import validateDNI from '@/utils/validateDni';

@Component
export default class ValidationsMixin extends Vue {

    EMAIL_REGEX: any = /\S+@\S+\.\S+/;

    // /**
    //  * @override
    //  */
    // mounted() {
    //     console.log('[VALIDATIONS] mounted');
    // }

    // /**
    //  * @override
    //  */
    // data() {
    //     return {
    //         DNI_REGEX: /^[0-9]{8,8}[A-Za-z]$/g,
    //         CIF_REGEX: /^([ABCDEFGHJKLMNPQRSUVW])([0-9]{7})([0-9A-J])$/,
    //         NIE_REGEX: /^[XYZ]{1}[0-9]{7,8}[A-Za-z]$/g,
    //         EMAIL_REGEX: /\S+@\S+\.\S+/
    //     };
    // }

    // methods: {
    /**
     * VALIDACIÓN DE DOCUMENTOS
     * Le pasamos un string con el valor que queremos validar y la función se encarga
     * de determinar qué tipo de documento es.
     * @param {String} str Valor que queremos validar
     * @param {String} type Tipo de documento a validar
     * @return {Object} Devuelve un objeto con la respuesta
     */
    validateSpanishID(str: any, type: any) {
        // Ensure upcase and remove whitespace
        str = str.toUpperCase().replace(/\s/, '');
        let valid = false;
        // Quitado, pasamos el tipo como parámetro para marcar como válido si no es [NIF | NIE | CIF]
        // const type = this.spainIdType(str);
        switch (type) {
            case 'NIF':
            case 'DNI':
                valid = this.validDNI(str);
                break;
            case 'NIE':
                valid = this.validNIE(str);
                break;
            case 'CIF':
                valid = this.validCIF(str);
                break;
            default:
                valid = true;
                break;
        }
        return {
            type,
            valid
        };
    }


    /**
     * VALIDACIÓN DE DNI
     * @param {String} dni DNI para validar
     * @return {*} respuesta
     */
    validDNI(dni: any) {
        return validateDNI(dni);
    }

    /**
     * VALIDACIÓN DE NIE
     * @param {String} nie NIE para validar
     * @return {*} respuesta
     */
    validNIE(nie: any) {
        if (spainIdType(nie) !== 'NIE') {
            return false;
        }
        // Change the initial letter for the corresponding number and validate as DNI
        let nie_prefix = nie.charAt(0);

        switch (nie_prefix) {
            case 'X':
                nie_prefix = 0;
                break;
            case 'Y':
                nie_prefix = 1;
                break;
            case 'Z':
                nie_prefix = 2;
                break;
            default:
                break;
        }

        return this.validDNI(nie_prefix + nie.substr(1));
    }

    /**
     * VALIDACIÓN DE CIF
     * @param {String} cif CIF para validar
     * @return {*} respuesta
     */
    validCIF(cif: any) {
        return validateCIF(cif);
    }

    /**
     * Validación de email
     * @param {String} email to validate
     * @return {Boolean} valid or not
     */
    validateEmail(email: string) {
        return this.EMAIL_REGEX.test(String(email).toLowerCase());
    }
    // }
}
