const DNI_REGEX: any = /^[0-9]{8,8}[A-Za-z]$/g;
const CIF_REGEX: any = /^([ABCDEFGHJKLMNPQRSUVW])([0-9]{7})([0-9A-J])$/;
const NIE_REGEX: any = /^[XYZ]{1}[0-9]{7,8}[A-Za-z]$/g;

/**
 * Comprueba tipo de documento
 * @param {String} str Valor que queremos validar
 * @return {String} Devuelve tipo de documento
 */
export default function spainIdType(str: any) {
    if (str.match(DNI_REGEX)) {
        return 'NIF';
    }
    if (str.match(CIF_REGEX)) {
        return 'CIF';
    }
    if (str.match(NIE_REGEX)) {
        return 'NIE';
    }
}