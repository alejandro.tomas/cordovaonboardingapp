import spainIdType from './checkSpanishIdType';

export default function validateDNI(dni: string) {
    if (spainIdType(dni) !== 'NIF') {
        return false;
    }
    const dni_letters = 'TRWAGMYFPDXBNJZSQVHLCKE';
    const letter = dni_letters.charAt(parseInt(dni, 10) % 23);

    return letter === dni.charAt(8).toUpperCase();
}