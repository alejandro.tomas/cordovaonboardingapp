import spainIdType from './checkSpanishIdType';

const CIF_REGEX: any = /^([ABCDEFGHJKLMNPQRSUVW])([0-9]{7})([0-9A-J])$/;
/**
 * VALIDACIÓN DE CIF
 * @param {String} cif CIF para validar
 * @return {*} respuesta
 */
export default function validateCIF(cif: any) {
    if (spainIdType(cif) !== 'CIF') {
        return false;
    }
    const match = cif.match(CIF_REGEX);
    const letter = match[1];
    const number = match[2];
    const control = match[3];

    let even_sum: any = 0;
    let odd_sum: any = 0;
    let n;

    for (let i = 0; i < number.length; i++) {
        n = parseInt(number[i], 10);

        // Odd positions (Even index equals to odd position. i=0 equals first position)
        if (i % 2 === 0) {
            // Odd positions are multiplied first.
            n *= 2;

            // If the multiplication is bigger than 10 we need to adjust
            odd_sum += n < 10 ? n : n - 9;

            // Even positions
            // Just sum them
        } else {
            even_sum += n;
        }
    }

    const control_digit = 10 - (even_sum + odd_sum).toString().substr(-1);
    const control_letter = 'JABCDEFGHI'.substr(control_digit, 1);

    // Control must be a digit
    if (letter.match(/[ABEH]/)) {
        return Number(control) === control_digit;
    } else if (letter.match(/[KPQS]/)) {
        return control === control_letter;
    } else {
        return Number(control) === control_digit || control === control_letter;
    }
}
