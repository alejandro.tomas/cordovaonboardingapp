import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import authService from "@/plugins/services/auth.service";
import servicesPlugin from "@/plugins/services/index";

import OnBoardingCore from "@/plugins/core";
import i18n from "./i18n";
import VueMonthlyPicker from "vue-monthly-picker";
import moment from "moment";
moment.locale("es");

// Plugin global services
Vue.use(servicesPlugin, { router });

// Core Plugin
Vue.use(OnBoardingCore);

// Components
Vue.component("month-picker", VueMonthlyPicker);

// Filters
Vue.filter("moment", function(date: Date, format: string) {
  return moment(date).format(format);
});

Vue.config.productionTip = false;

import VueCordova from "vue-cordova";
Vue.use(VueCordova, {
  optionTestKey: "optionTestValue",
});

// add cordova.js only if serving the app through file://
if (window.location.protocol === "file:" || window.location.port === "3000") {
  var cordovaScript = document.createElement("script");
  cordovaScript.setAttribute("type", "text/javascript");
  cordovaScript.setAttribute("src", "cordova.js");
  document.body.appendChild(cordovaScript);

  document.addEventListener("deviceready", onDeviceReady, false);
  function onDeviceReady() {
    console.log("Cordova : device is ready !");

    console.warn("Plugin camera");
    console.log((navigator as any).camera);

    console.warn("Plugin diagnostic");
    // console.log((navigator as any).diagnostic);
    // @ts-ignore
    cordova.plugins.diagnostic.requestCameraAuthorization({
      successCallback: function(status) {
        console.log(
          "Authorization request for camera use was " +
            // @ts-ignore
            (status == cordova.plugins.diagnostic.permissionStatus.GRANTED
              ? "granted"
              : "denied")
        );
      },
      errorCallback: function(error) {
        console.error(error);
      },
      externalStorage: false,
    });
  }
}

authService
  .authenticate()
  .then(() =>
    new Vue({
      i18n,
      router,
      store,
      render: (h) => h(App),
    }).$mount("#app")
  )
  .catch((err) => console.error(err));
