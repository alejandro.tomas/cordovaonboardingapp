import { Component, Vue, Prop } from 'vue-property-decorator'
@Component
export default class YuriSpinner extends Vue {
    @Prop() readonly text!: string;
}