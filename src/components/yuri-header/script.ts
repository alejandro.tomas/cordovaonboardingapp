import { Component, Vue, Prop } from 'vue-property-decorator'
@Component
export default class YuriHeader extends Vue {
    @Prop() readonly title!: string;
    @Prop() readonly tag!: string;
    @Prop({ required: false, type: Boolean, default: false })
    readonly img!: boolean;
}