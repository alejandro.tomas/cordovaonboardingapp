import { Component, Vue, Prop } from 'vue-property-decorator'
@Component
export default class AgentBox extends Vue {
    @Prop() readonly agent!: any;

    get hasSocialLinks(): boolean {
        return this.agent.linkedin || this.agent.facebook || this.agent.twitter || this.agent.instagram
    }

}