/* eslint-disable  @typescript-eslint/no-explicit-any */
const components: any = {
    /**
     * @override
     */
    YuriHeader() {
        return import(/* webpackChunkName: "YuriHeader" */ '@/components/yuri-header/index.vue');
    },
    /**
     * @override
     */
    AgentBox() {
        return import(/* webpackChunkName: "AgentBox" */ '@/components/agent-box/index.vue');
    },
    /**
     * @override
     */
    Multiselect() {
        return import(/* webpackChunkName: "Multiselect" */ 'vue-multiselect');
    },
    /**
     * @override
     */
    Spinner() {
        return import(/* webpackChunkName: "Spinner" */ '@/components/yuri-spinner/index.vue');
    },
    /**
     * @override
     */
    Stepper() {
        return import(/* webpackChunkName: "Stepper" */ '@/components/stepper/index.vue');
    }
};

export default components;
