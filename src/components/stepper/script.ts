import { Component, Vue, Prop } from 'vue-property-decorator'
import StepperStep from '@/interfaces/StepperStep';
@Component
export default class Stepper extends Vue {
    @Prop({ required: true }) readonly steps!: StepperStep[];
}