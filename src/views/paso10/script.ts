/* eslint-disable  @typescript-eslint/no-non-null-assertion */
import { Component, Vue } from 'vue-property-decorator';
import OnboardingState from '@/interfaces/OnboardingState';
import User from '@/interfaces/User';
import OnboardingStateBody from '@/interfaces/OnboardingStateBody';
import { AxiosResponse } from 'axios';
import Step10State from '@/interfaces/Step10State';

@Component({})
export default class Step10 extends Vue {
  lead_uuid = '';
  username = '';
  password = '';
  confirm = '';
  mobile_phone = '';
  loading = false;

  created() {
    const data: OnboardingStateBody = this.$store.getters['applicationStore/applicationData'];
    this.lead_uuid = data.lead_uuid;
    this.username = data.step2.email;
    this.mobile_phone = data.step2.mobile_phone_prefix + data.step2.mobile_phone;
  }

  async handleNext() {
    const payload: { step: string; data: Step10State } = {
      step: 'step10',
      data: { username: this.username, password: this.password, mobile_phone: this.mobile_phone }
    };
    this.loading = true;
    try {
      await this.$store.dispatch('applicationStore/setDataStep', payload);
      const stateData: OnboardingStateBody = this.$store.getters['applicationStore/applicationData'];
      await this.createUser(stateData);
      const newOnboardingState: OnboardingState = { state_id: this.lead_uuid, actual_step: 10, state_json: stateData };
      await this.$onboardingService.updateOnboardingState(newOnboardingState);
      this.$router.push('/paso11');

    } catch (error) {
      console.error(error);
      this.$bvToast.toast(`${error.message}`, {
        title: `Error`,
        autoHideDelay: 10000,
        variant: 'danger'
      });
    }
    this.loading = false;
  }

  createUser(state_json: OnboardingStateBody): Promise<AxiosResponse> {
    const newUser: User = {
      username: this.username,
      password: this.password,
      given_name: state_json.step2.name,
      family_name: state_json.step2.lastname_1
    };
    return this.$onboardingService.createUser(newUser);
  }
}
