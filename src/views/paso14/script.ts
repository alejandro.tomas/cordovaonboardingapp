/* eslint-disable  @typescript-eslint/no-explicit-any */
import { Component, Mixins } from 'vue-property-decorator';
import StepsMixin from '@/mixins/steps';
import OnboardingStateBody from '@/interfaces/OnboardingStateBody';
import PhysicalPersonData from '@/interfaces/PhysicalPersonData';
import { AxiosResponse } from 'axios';
import Step14State from '@/interfaces/Step14State';

@Component
export default class Step14 extends Mixins(StepsMixin) {
  lead_uuid = '';
  isJuridicalEntity = false;
  interveners: PhysicalPersonData[] = [];
  operations: any[] = [];
  info: any;
  kyc: any = {};
  postal_address: any;
  loading = false;
  selectedOperationIndex = 0;

  created() {
    const data: OnboardingStateBody = this.$store.getters['applicationStore/applicationData'];

    this.lead_uuid = data.lead_uuid;
    this.isJuridicalEntity = data.isJuridicalEntity;
    this.interveners = data.interveners;
    this.operations = data.operations;
    this.info = data.step2;
    this.kyc = data.step8;
    this.postal_address = this.interveners[0].addresses.postal_address;

    if (this.interveners.length > 0 && this.info !== undefined) {
      this.interveners[0].email = this.info.email;
      this.interveners[0].mobile_phone = this.info.mobile_phone;
      this.interveners[0].key_mobile_phone_prefix = this.info.mobile_phone_prefix;
    }
  }

  async handleNext() {
    this.loading = true;
    const aliasAccount = Date.now();
    const intervenersFormatted = this.formatInterveners();
    const payload = {
      alias_account: `Alias ${aliasAccount}`,
      signature_type: 'SOLIDARIA',
      is_withholding_tax_exempt: false,
      is_transfers_blocked: false,
      interveners: intervenersFormatted,
      uuid_distributor: '6b4e3e9b-0e7f-494a-bdc9-0818eaf61e02',
      source: 'ONLINE',
      uuid_subdistributor: '6b4e3e9b-0e7f-494a-bdc9-0818eaf61e21'
    };

    try {
      const accountReponse = await this.$onboardingService.createAccount(payload);
      const accountData = accountReponse.data.account;

      // Guardamos la información de la cuenta en el store
      await this.$store.dispatch('applicationStore/setDataStep', { step: 'account', data: accountData });

      // Guardamos las respuestas KYC y creamos las operaciones sobre la cuenta creada
      await Promise.all([
        this.sendKycResponses(accountReponse),
        this.createOperations(accountReponse)
      ]).then(res => {
        const operationsCreated = res[1].map((operation: any) => operation.data.operation);
        const stepData: { step: string; data: Step14State } = {
          step: 'step14',
          data: {
            accountCreated: accountReponse.data.account,
            operationsCreated: operationsCreated
          }
        };
        this.$store.dispatch('applicationStore/setDataStep', stepData);
        this.$bvToast.toast(`Cuenta y operaciones creadas con éxito.`, {
          title: `OK`,
          autoHideDelay: 8000
        });
        // Guardamos el estado del lead y avanzamos un paso
        this.updateOnboardingState(this.lead_uuid, 14);
      });

    } catch (error) {
      console.error(error);
      this.$bvToast.toast(`No se ha podido crear la cuenta/operación.`, {
        title: `Error`,
        autoHideDelay: 8000,
        variant: 'danger'
      });
    }
    this.loading = false;
  }

  sendKycResponses(accountResponse: AxiosResponse<any>): Promise<any> {
    const responsePromises: any[] = [];
    for (const elem in this.kyc) {
      this.kyc[elem].uuid_intervener = accountResponse.data.account.fiscal_address.uuid_intervener;
      responsePromises.push(this.$onboardingService.sendKycResponses(this.kyc[elem]));
    }
    return Promise.all(responsePromises);
  }

  createOperations(res: any): Promise<any> {
    const uuidAccount = res.data.account.uuid;
    const uuidLeadIntervener = res.data.account.fiscal_address.uuid_intervener;
    const operationsPromises: any = [];

    this.operations.forEach((operation: any) => {
      const payload = {
        id_object: this.generateIDObject(),
        uuid_account: uuidAccount,
        uuid_ordering: uuidLeadIntervener,
        key_operation_type: operation.selectedOperation.key,
        key_product: operation.selectedProduct.key,
        uuid_distributor: '6b4e3e9b-0e7f-494a-bdc9-0818eaf61e02',
        uuid_subdistributor: '6b4e3e9b-0e7f-494a-bdc9-0818eaf61e21',
        source: operation.operationData.source.key,
        notify_operation: false,
        amount_shares_indicator: 'B',
        gross_amount: +operation.operationData.grossAmount,
        per_increase:
          operation.operationData.per_increase !== undefined ? operation.operationData.per_increase : undefined,
        per_increase_period:
          operation.operationData.per_increase_period !== undefined &&
            operation.operationData.per_increase_period.key !== undefined
            ? operation.operationData.per_increase_period.key
            : undefined,
        per_increase_type:
          operation.operationData.per_increase_type !== undefined &&
            operation.operationData.per_increase_type &&
            operation.operationData.per_increase_type.key !== 'no-increase'
            ? operation.operationData.per_increase_type.key
            : undefined,
        per_period:
          operation.operationData.per_period !== undefined && operation.operationData.per_period.key !== undefined
            ? operation.operationData.per_period.key
            : undefined,
        per_start_date:
          operation.operationData.per_start_date !== undefined ? operation.operationData.per_start_date : undefined,
        per_end_date:
          operation.operationData.per_end_date !== undefined ? operation.operationData.per_end_date : undefined
      };
      operationsPromises.push(this.$onboardingService.createOperations(payload));
    });

    return Promise.all(operationsPromises);
  }

  formatInterveners() {
    return this.interveners.map((x: any) => {
      let document_type = x.document_type.key || x.document_type || null;
      if (document_type) {
        document_type = document_type.toUpperCase();
      }
      let document = null;
      switch (document_type) {
        case 'NIF':
          document = 'DNI';
          break;
        case 'CIF':
          document = 'CIF';
          break;
        default:
          document = document_type;
          break;
      }
      let x_roles = x.roles;
      if (Array.isArray(x_roles) && x_roles.length == 1 && x_roles[0].key !== undefined) {
        x_roles = x_roles[0].key;
      } else if (Array.isArray(x_roles) && x_roles.length > 1 && x_roles[0].key !== undefined) {
        x_roles = x_roles.map((role: any) => role.key);
      }

      if (x.uuid !== undefined) {
        return {
          account_relationship: {
            account_intervener_role: x.roles.key || x.roles,
            holding_percentage: x.percent || 0
          },
          href: `${process.env.VUE_APP_API_URL}/interveners-api/interveners/${x.uuid}`
        };
      } else {
        return {
          personal_data: {
            name: x.name,
            lastname_1: x.lastname_1,
            lastname_2: x.lastname_2,
            intervener_type: x.intervener_type,
            birthdate: x.birthdate,
            document_expiration_date: x.document_date || null,
            document_type: document,
            key_nationality_1: x.nationality.key,
            key_nationality_2: null,
            key_country_of_birth: x.place_of_birth.key,
            city_of_birth: 'Prueba',
            gender: x.gender,
            marital_status: x.marital_status && x.marital_status.key,
            matrimonial_regime: null,
            document_id: x.document_number || null,
            family_name: ''
          },
          contacts: {
            email: x.email === undefined ? null : x.email,
            key_mobile_phone_prefix: x.key_mobile_phone_prefix === undefined ? null : x.key_mobile_phone_prefix,
            mobile_phone: x.mobile_phone === undefined ? null : x.mobile_phone
          },
          fiscal_address: {
            f_address_street_type: x.addresses.fiscal_address.street_type.key,
            f_address_street_1: x.addresses.fiscal_address.street_1,
            f_address_street_2: null,
            f_address_number: x.addresses.fiscal_address.number,
            f_address_floor: x.addresses.fiscal_address.floor,
            f_address_door: x.addresses.fiscal_address.door,
            f_address_postal_code: x.addresses.fiscal_address.postal_code,
            f_address_city: x.addresses.fiscal_address.city,
            f_address_key_country: x.country.key || '',
            f_address_state: x.addresses.fiscal_address.state.key || ''
          },
          postal_address: [
            {
              street_type: x.addresses.postal_address.street_type.key,
              street_1: x.addresses.postal_address.street_1,
              street_2: null,
              number: x.addresses.postal_address.number,
              floor: x.addresses.postal_address.floor,
              door: x.addresses.postal_address.door,
              postal_code: x.addresses.postal_address.postal_code,
              city: x.addresses.postal_address.city,
              key_country: x.country.key || '',
              state: x.addresses.postal_address.state.key || ''
            }
          ],
          business: {},
          crs: {},
          fatca: {},
          account_relationship: {
            account_intervener_role: x_roles,
            holding_percentage: x.percent || 0
          }
        };
      }
    });
  }

  removeOperation() {
    this.$store.dispatch('applicationStore/removeOperation', this.selectedOperationIndex);
    this.updateOnboardingState(this.lead_uuid, 13);
    this.$bvModal.hide('modal');
    this.$bvToast.toast(`La operación ha sido eliminada.`, {
      title: `Operación eliminada`
    });
  }

  //TODO: Move to utils or similar
  generateIDObject() {
    const randomString = Math.random()
      .toString(36)
      .substr(2, 12);
    const id_object = `OP_${randomString}`;
    return id_object;
  }
}
