export default interface Response {
    id_poll: number;
    id_question: number;
    uuid_intervener: string;
    answer: Array<any> | any;
    answer_date: Date;
}