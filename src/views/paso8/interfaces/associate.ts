export default interface Associate {
    name: string;
    surname: string;
    nif: string;
    per: number;
    test: boolean;
}