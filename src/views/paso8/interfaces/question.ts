interface Answer {
  type: string;
  question: string;
  responses?: string;
}

type ArrAnswer = Array<Answer>;

export default interface Question {
  id_poll: number;
  id_question: number;
  poll_type: string;
  question: string;
  responses?: any;
}