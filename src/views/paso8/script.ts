import { Component, Vue } from 'vue-property-decorator'
import Question from './interfaces/question';
import Response from './interfaces/response';
import Associate from './interfaces/associate';
import { AxiosResponse } from 'axios';
import { LocaleMessage } from 'vue-i18n';
import OnboardingStateBody from '@/interfaces/OnboardingStateBody';
import OnboardingState from '@/interfaces/OnboardingState';
import router from '@/router';
import PhysicalPersonData from '@/interfaces/PhysicalPersonData';
import moment from "moment";

@Component
export default class Step8 extends Vue {
  questions: Question[] = [];
  responses: Response[] = [];
  countries = [];
  entityType = '';
  condition = '';
  loading = false;
  associates: Associate[] = [
    {
      name: '',
      surname: '',
      nif: '',
      per: 0,
      test: false
    }
  ];
  patrons: Associate[] = [
    {
      name: '',
      surname: '',
      nif: '',
      per: 0,
      test: false
    }
  ];
  responseee = '';
  private intervener_uuid: undefined | string = undefined;
  private lead_uuid = '';

  isAnyAssociateNotValid(type: string): boolean {
    const collection = (type === 'associate') ? this.associates : this.patrons;
    return collection.some(associate => (associate.name === null || associate.surname === null || associate.nif === null || associate.per === null) ||
      (associate.name === '' || associate.surname === '' || associate.nif === '') ||
      ('TRWAGMYFPDXBNJZSQVHLCKE'.charAt(parseInt(associate.nif, 10) % 23) !== associate.nif.charAt(8).toUpperCase()));
  }

  created() {
    this.loadDataFromStore();
    this.$onboardingService.getCountries()
      .then((res: any) => {
        const countriesList = res.countries_info;
        const i18nCountries: any = this.$t(`countries`);
        this.countries = countriesList.map((x: any) => {
          return { name: i18nCountries[x.country_key], key: x.country_key };
        })
        this.loadForm(this.entityType);
      });
  }

  loadDataFromStore() {
    const data = this.$store.getters['applicationStore/applicationData']
    this.entityType = data.isJuridicalEntity === true ? 'CONOCIMIENTO_JURIDICA' : 'CONOCIMIENTO_FISICA';
    this.intervener_uuid = data.uuid_intervener;
    this.lead_uuid = data.lead_uuid;
  }

  private que(question: string): LocaleMessage {
    return this.$t(`kyc.questions.${question}`);
  }

  private res(response: string): LocaleMessage {
    return this.$t(`kyc.responses.${response}`);
  }

  onSave(valid: boolean) {
    if (valid) {
      this.loading = !this.loading;
      if (this.entityType === 'CONOCIMIENTO_JURIDICA') {
        if (this.condition && this.condition === 'SI') {
          this.responses[8].answer[0] = this.patrons;
        }
        this.responses[7].answer[0] = this.associates;
        this.saveInterveners();
      }
      const answer_date = new Date();
      for (let i = 0; i < this.responses.length; i++) {
        this.responses[i].answer_date = answer_date;
      }
      const payload = {
        step: 'step8',
        data: this.responses
      };
      this.$store.dispatch('applicationStore/setDataStep', payload)
        .then(() => {
          const stateData: OnboardingStateBody = this.$store.getters['applicationStore/applicationData'];
          const newOnboardingState: OnboardingState = { state_id: this.lead_uuid, actual_step: 8, state_json: stateData };
          this.$onboardingService.updateOnboardingState(newOnboardingState).then(() => {
            this.loading = false;
            router.push('/paso9');
          })
        })
        .catch(err => {
          this.loading = false;
          console.error(err)
          this.$bvToast.toast(`${err.message}`, {
            title: `Error`,
            autoHideDelay: 10000,
            variant: 'danger'
          });
        });
    }
  }

  saveInterveners() {
    for (const patron of this.patrons) {
      if (patron.name && patron.nif && patron.surname) {
        const intervener = this.mapAssociateToIntervener(patron);
        intervener.roles = [{ key: "PATRONO", name: "Patrono" }];
        this.$store.dispatch('applicationStore/setIntervener', intervener);
      }
    }
    for (const associate of this.associates) {
      if (associate.name && associate.nif && associate.surname) {
        const intervener = this.mapAssociateToIntervener(associate);
        intervener.roles = (intervener.percent || 0) >= 25 ? [{ key: "TITULAR_REAL", name: "Titular real" }] : [{ key: "ADMINISTRADOR", name: "Administrador" }];
        this.$store.dispatch('applicationStore/setIntervener', intervener);
      }
    }
  }

  mapAssociateToIntervener(associate: Associate): PhysicalPersonData {
    return {
      intervener_type: 'F',
      name: associate.name,
      lastname_1: associate.surname,
      lastname_2: '',
      birthdate: moment(new Date()).format("YYYY-MM-DD"),
      place_of_birth: {
        key: "ESP",
        name: "España"
      },
      country: {
        key: "ESP",
        name: "España"
      },
      nationality: {
        key: "ESP",
        name: "España"
      },
      gender: 'V',
      document_type: "DNI",
      document_number: associate.nif,
      document_date: '',
      is_document_permanent: true,
      percent: associate.per,
      addresses: {
        fiscal_address: {
          city: "Castellon",
          door: "C",
          floor: "1",
          key_country: "ESP",
          number: "1",
          postal_code: "12001",
          state: "ESP-CS",
          street_type: "CL",
          street_1: "Avinguda de Vicent Sos Baynat"
        },
        postal_address: {
          city: "Castellon",
          door: "C",
          floor: "1",
          key_country: "ESP",
          number: "1",
          postal_code: "12001",
          state: "ESP-CS",
          street_type: "CL",
          street_1: "Avinguda de Vicent Sos Baynat"
        }
      },
      kyc: true,
      secondary: true
    };
  }

  onChangeClearResponse(index: number) {
    setTimeout(() => {
      if (this.entityType === 'CONOCIMIENTO_FISICA') {
        if (index === 0) {
          this.responses[0].answer = [this.responses[0].answer[0]];
        }
        if (index === 2) {
          if (!this.responses[2].answer[0].includes('ABOGADOS_OTROS')) {
            this.responses[4].answer = [];
          }
          if (!this.responses[2].answer[0].includes('OTROS')) {
            this.responses[3].answer = [];
          }
        }
        if (index === 5) {
          this.responses[5].answer = [this.responses[5].answer[0]];
        }
        if (index === 6) {
          this.responses[7].answer = [];
        }
        if (index === 10) {
          this.responses[10].answer = [this.responses[10].answer[0]];
        }
        if (index === 11) {
          this.responses[11].answer = [this.responses[11].answer[0]];
        }
      } else {
        if (index === 0) {
          if (!this.responses[0].answer[0].includes('ABOGADOS_OTROS')) {
            this.responses[0].answer = [this.responses[0].answer[0]];
          }
          if (!this.responses[0].answer[0].includes('OTROS')) {
            this.responses[1].answer = [];
          }
        }
        if (index === 6) {
          this.responses[6].answer = [this.responses[6].answer[0]];
        }
        if (index === 8) {
          this.patrons = [
            {
              name: '',
              surname: '',
              nif: '',
              per: 0,
              test: false
            }
          ];
          this.responses[8].answer[0] = [];
        }
        if (index === 9) {
          this.responses[9].answer = [this.responses[9].answer[0]];
        }
        if (index === 10) {
          this.responses[10].answer = [this.responses[10].answer[0]];
        }
      }
    }, 10);
  }

  loadForm(type: string) {
    this.entityType = type;
    this.questions = [];
    this.responses = [];
    this.$onboardingService.getKycQuestions()
      .then((res: AxiosResponse) => {
        this.questions = res.data.poll.filter((qs: Question) => {
          return qs.poll_type === type;
        }).sort((a: Question, b: Question) => {
          return a.id_question > b.id_question;
        });
        if (!this.responses.length) {
          this.responses = this.questions.map((question: Question) => {
            return {
              id_poll: question.id_poll,
              id_question: question.id_question,
              uuid_intervener: this.intervener_uuid,
              answer: []
            } as Response;
          });
        }
      })
      .catch((err: Error) => {
        console.error(err);
      });
  }
}
