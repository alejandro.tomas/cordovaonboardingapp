import { Component, Mixins } from 'vue-property-decorator';
import Question from './interfaces/question';
import Response from '../../interfaces/Response';
import { AxiosResponse } from 'axios';
import StepsMixin from '@/mixins/steps';

@Component
export default class Paso3 extends Mixins(StepsMixin) {
  questions: Question[] = [];
  responses: Response[] = [];
  lead_uuid = '';
  graphImage = require('../../assets/images/test-graph.jpg');
  graphImage2 = require('../../assets/images/test-graph2.jpg');
  loading = false;

  get isAnyResponseEmpty(): boolean {
    return this.responses.some(response => response.selected_response === undefined);
  }

  created() {
    this.loadDataFromStore();
    this.$onboardingService
      .getTestQuestions()
      .then((res: AxiosResponse) => {
        this.questions = res.data.questions;

        if (!this.responses.length) {
          this.responses = res.data.questions.map((question: Question) => {
            // TODO: cambiar uuid_lead_client a pincho
            return {
              uuid_question: question.uuid,
              uuid_lead_client: this.lead_uuid
            } as Response;
          });
        }
      })
      .catch((err: Error) => console.error(err));
  }

  loadDataFromStore() {
    const data = this.$store.getters['applicationStore/applicationData'];
    this.lead_uuid = data.lead_uuid;
    const dataStep = data.step3;
    if (dataStep && dataStep.length) {
      this.responses = dataStep.responses;
    }
  }

  handleNext() {
    if (!this.isAnyResponseEmpty && !this.loading) {
      this.loading = true;
      this.$onboardingService.submitTest(this.responses)
        .then((res: AxiosResponse) => {
          const payload = {
            step: 'step3',
            data: { responses: this.responses, failed: res.data.failed }
          };
          this.$store.dispatch('applicationStore/setDataStep', payload);
          this.updateOnboardingState(this.lead_uuid, 3);
          setTimeout(() => {
            this.loading = false;
          }, 200);
        })
        .catch((err: Error) => {
          this.loading = false;
          console.error(err);
          this.$bvToast.toast(`${err.message}`, {
            title: `Error`,
            autoHideDelay: 10000,
            variant: 'danger'
          });
        });
    }
  }
}
