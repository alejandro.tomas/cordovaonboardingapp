interface Answer {
  uuid_question: string;
  num_answer: number;
  answer_text: string;
}

export default interface Question {
  uuid: string;
  question_order: number;
  question_text: string;
  answers: Answer[];
}