import { Component, Vue } from 'vue-property-decorator';
import router from '@/router';
import OnboardingState from '@/interfaces/OnboardingState';

@Component({})
export default class FinTest extends Vue {
    failed = false;
    failedAndContinued = false;
    lead_uuid = '';

    created() {
        const data = this.$store.getters['applicationStore/applicationData'];
        this.lead_uuid = data.lead_uuid;
        this.failed = data.step3.failed;
    }

    handleStart() {
        this.failedAndContinued = true;
        this.failed = false;
    }

    handleEntitySelection(isJuridicalEntity: boolean) {
        this.$store.dispatch('applicationStore/setIsJuridicalEntity', { isJuridicalEntity: isJuridicalEntity, settedIsJuridicalEntity: true });
        // const payload = {
        //     step: 'step4',
        //     data: { isJuridicalEntity: isJuridicalEntity }
        // };
        // this.$store.dispatch('applicationStore/setDataStep', payload);
        this.updateOnboardingState();
        if (isJuridicalEntity && !this.addIntervener()) {
            router.push('/paso6');
        } else {
            router.push('/paso5');
        }
    }

    updateOnboardingState() {
        const data = this.$store.getters['applicationStore/applicationData'];
        const newOnboardingState = { state_id: this.lead_uuid, actual_step: 4, state_json: data } as OnboardingState;
        this.$onboardingService.updateOnboardingState(newOnboardingState)
            .catch((err: Error) => console.error(err));
    }

    addIntervener() {
        const data = this.$store.getters['applicationStore/applicationData'];
        return data.step6 && data.step6.name;
    }
}