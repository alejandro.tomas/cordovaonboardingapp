/* eslint-disable  @typescript-eslint/no-explicit-any */
import { Component, Vue, Prop } from 'vue-property-decorator';

@Component({})
export default class PpTransferFields extends Vue {
    @Prop({ required: true })
    operationData!: any;
    @Prop({ required: true })
    amountSharesIndicatorOptions!: any[];
    @Prop({ required: true })
    selectedProduct!: any;

    planTypeOptions: any[] = [];
    sourceSituationOptions: any[] = [];
    compartmentOptions: any[] = [];
    rightTypeOptions: any[] = [];

    created() {
        this.loadPlanTypeOptionsOptions();
        this.loadSourceSituationOptions();
        this.loadCompartmentOptions();
        this.loadRightTypeOptions();
    }

    loadPlanTypeOptionsOptions() {
        const retirementPlanTypes = this.$t(`operations.retirement_plan_type`);
        this.planTypeOptions = Object.entries(retirementPlanTypes).map(item => {
            return {
                key: item[0],
                name: item[1]
            };
        });
    }

    loadSourceSituationOptions() {
        const sourceSituations = this.$t(`operations.source_situations`);
        this.sourceSituationOptions = Object.entries(sourceSituations).map(item => {
            return {
                key: item[0],
                name: item[1]
            };
        });
    }

    loadCompartmentOptions() {
        const compartments = this.$t(`operations.compartments`);
        this.compartmentOptions = Object.entries(compartments).map(item => {
            return {
                key: item[0],
                name: item[1]
            };
        });
    }

    loadRightTypeOptions() {
        const rightTypes = this.$t(`operations.rights_types`);
        this.rightTypeOptions = Object.entries(rightTypes).map(item => {
            return {
                key: item[0],
                name: item[1]
            };
        });
    }

    handleOperationInputTypeChange() {
        this.operationData.grossAmount = null;
        this.operationData.shares = null;
    }

    handleInputShares() {
        this.operationData.grossAmount = (this.operationData.shares * this.selectedProduct.net_asset_value).toFixed(6);
    }

    handleInputGrossAmount() {
        this.operationData.shares = (this.operationData.grossAmount / this.selectedProduct.net_asset_value).toFixed(6);
    }
}