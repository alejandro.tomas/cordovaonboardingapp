/* eslint-disable  @typescript-eslint/no-explicit-any */
import { Component, Vue, Prop } from 'vue-property-decorator';
import moment from "moment";

@Component({})
export default class PeriodicFields extends Vue {
    @Prop({ required: true })
    operationData!: any;
    @Prop({ required: true })
    productType!: string;
    @Prop({ required: true })
    selectedOperation!: any;
    periodicityOptions: any[] = [];
    revaluationOptions: any[] = [];

    created() {
        this.loadPeriodicityOptions();
        this.loadRevaluationOptions();
    }

    loadPeriodicityOptions() {
        const periodicity = this.$t(`operations.periodicity_options`);
        this.periodicityOptions = Object.entries(periodicity).map(item => {
            return {
                key: item[0],
                name: item[1]
            };
        });
    }

    loadRevaluationOptions() {
        const revaluations = this.$t(`operations.revaluation_options`);
        this.revaluationOptions = Object.entries(revaluations).map(item => {
            return {
                key: item[0],
                name: item[1]
            };
        });
    }

    get minStartDate() {
        let minimalDate = moment();
        if (this.productType === 'FI') {
            minimalDate.format('YYYY-MM-DD');
        }
        if (this.selectedOperation.key === 'APORTACION_PERIODICA') {
            const today = moment();
            const todayDay = today.date();
            let monthsToAdd = 1;
            if (todayDay > 25) {
                monthsToAdd = 2;
            }
            minimalDate = moment().add(monthsToAdd, 'months');
        }
        return minimalDate;
    }

    get minEndDate() {
        let monthsToAdd = 0;
        let start = moment();
        if (this.operationData.per_start_date) {
            start = moment(this.operationData.per_start_date);
        }
        if (this.operationData.per_period) {
            switch (this.operationData.per_period.key) {
                case 'MENSUAL':
                    monthsToAdd = 1;
                    break;
                case 'TRIMESTRAL':
                    monthsToAdd = 3;
                    break;
                case 'SEMESTRAL':
                    monthsToAdd = 6;
                    break;
                case 'ANUAL':
                    monthsToAdd = 12;
                    break;
                default:
                    break;
            }
        }
        return start.add(monthsToAdd, 'months');
    }

    get periodicityIncreaseOptions() {
        if (this.productType === 'PP') {
            return [
                {
                    key: 'ANUAL', name: 'Anual'
                },
                {
                    key: 'BIANUAL', name: 'Binual'
                },
                {
                    key: 'TRIANUAL', name: 'Trianual'
                }
            ];
        } else {
            return [
                {
                    key: 'MENSUAL', name: 'Mensual'
                },
                {
                    key: 'TRIMESTRAL', name: 'Trimestral'
                },
                {
                    key: 'ANUAL', name: 'Anual'
                }
            ];
        }
    }
}