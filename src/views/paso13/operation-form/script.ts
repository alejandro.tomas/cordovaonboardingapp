/* eslint-disable  @typescript-eslint/no-explicit-any */
import { Component, Vue, Prop } from 'vue-property-decorator';
import SubscriptionFields from './subscription-fields/index.vue';
import PeriodicFields from './periodic-fields/index.vue';
import FiTransferFields from './fi-transfer-fields/index.vue';
import PpTransferFields from './pp-transfer-fields/index.vue';

@Component({
    components: {
        PeriodicFields,
        SubscriptionFields,
        FiTransferFields,
        PpTransferFields
    }
})
export default class OperationForm extends Vue {
    @Prop({ required: true })
    productType!: string;
    @Prop({ required: true })
    selectedOperation!: string;
    @Prop({ required: true })
    selectedProduct!: string;
    @Prop({ required: true })
    operationData!: any;
    amountSharesIndicatorOptions: any[] = [];

    created() {
        this.loadAmountSharesIndicatorOptionsOptions();
    }

    loadAmountSharesIndicatorOptionsOptions() {
        const indicators = this.$t(`operations.indicators`);
        this.amountSharesIndicatorOptions = Object.entries(indicators).map(item => {
            return {
                key: item[0],
                name: item[1]
            };
        });
    }
}