/* eslint-disable  @typescript-eslint/no-explicit-any */
import { Component, Vue, Prop } from 'vue-property-decorator';

@Component({})
export default class FiTransferFields extends Vue {
    @Prop({ required: true })
    operationData!: any;
    @Prop({ required: true })
    selectedProduct!: any;
    @Prop({ required: true })
    amountSharesIndicatorOptions!: any[];

    handleInputShares() {
        this.operationData.grossAmount = (this.operationData.shares * this.selectedProduct.net_asset_value).toFixed(6);
    }

    handleInputGrossAmount() {
        this.operationData.shares = (this.operationData.grossAmount / this.selectedProduct.net_asset_value).toFixed(6);
    }

    handleOperationInputTypeChange() {
        this.operationData.grossAmount = null;
        this.operationData.shares = null;
    }
}