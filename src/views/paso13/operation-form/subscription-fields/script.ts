/* eslint-disable  @typescript-eslint/no-explicit-any */
import { Component, Vue, Prop } from 'vue-property-decorator';

@Component({})
export default class SubscriptionFields extends Vue {
    @Prop({ required: true })
    selectedOperation!: any;
    @Prop({ required: true })
    operationData!: any;
    @Prop({ required: true })
    selectedProduct!: any;
    paymentMethodOptions: any[] = [];

    created() {
        this.loadPaymentOptions();
    }

    loadPaymentOptions() {
        const paymentMethods = this.$t(`operations.pay_methods`);
        this.paymentMethodOptions = Object.entries(paymentMethods).map(item => {
            return {
                key: item[0],
                name: item[1]
            };
        });
    }
}