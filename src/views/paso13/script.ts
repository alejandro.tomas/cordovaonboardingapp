/* eslint-disable  @typescript-eslint/no-explicit-any */
import { Component, Mixins } from 'vue-property-decorator';
import OnboardingStateBody from '@/interfaces/OnboardingStateBody';
import OperationForm from './operation-form/index.vue';
import Step13State from '@/interfaces/Step13State';
import { AxiosResponse } from 'axios';
import StepsMixin from '@/mixins/steps';
import PhysicalPersonData from '@/interfaces/PhysicalPersonData';
import moment from 'moment';

@Component({
  components: {
    OperationForm
  }
})
export default class Step13 extends Mixins(StepsMixin) {
  lead_uuid = '';
  productType = '';
  selectedOperation = '';
  selectedProduct: any = {};
  operationData: any = {};
  operationsOptions: any[] = [];
  productsOptions: any[] = [];
  products: any[] = [];
  isJuridicalEntity = false;
  interveners: PhysicalPersonData[] = [];
  PPDisabledReason = '';
  showProductTypeOptions = false;

  created() {
    const storeData: OnboardingStateBody = this.$store.getters['applicationStore/applicationData'];
    this.lead_uuid = storeData.lead_uuid;
    this.isJuridicalEntity = storeData.isJuridicalEntity;
    this.interveners = storeData.interveners;
    if (storeData.operationsType) {
      storeData.operationsType === "all" ? this.showProductTypeOptions = true : this.handleProductTypeSelection(storeData.operationsType);
    }
    if (this.isPPDisabled) {
      this.handleProductTypeSelection("FI");
    }
    this.loadProducts();
  }

  loadProducts() {
    this.$onboardingService
      .getProducts()
      .then((res: AxiosResponse<any>) => {
        res.data.products.forEach((product: any) => product.net_asset_value = (product.net_asset_value || 100))
        this.products = res.data.products;
        this.checkIfEditOperation();
      })
      .catch((err: Error) => console.error(err));
  }

  loadOperationsOptions(selectedType: string) {
    const operations = this.$t(`operations.operations_types.${selectedType}`);
    this.operationsOptions = Object.entries(operations).map(item => {
      return {
        key: item[0],
        name: item[1]
      };
    });
  }

  loadProductsOptions(selectedType: string) {
    const products = this.$t(`operations.products.${selectedType}`);
    this.productsOptions = Object.entries(products).map(item => {
      return {
        key: item[0],
        name: item[1]
      };
    });
  }

  handleProductTypeSelection(selectedType: string) {
    this.productType = selectedType;
    this.selectedOperation = '';
    this.selectedProduct = {};
    this.operationData = {};
    this.loadOperationsOptions(selectedType);
    this.loadProductsOptions(selectedType);
  }

  handleProductSelection(selectedProduct: any) {
    this.selectedProduct = this.products.find(product => (product.key_name === selectedProduct.key || product.key === selectedProduct.key));
    this.selectedProduct.name = selectedProduct.name;
    this.selectedProduct.key = selectedProduct.key;
    delete this.selectedProduct.key_name;
    delete this.selectedProduct._links;
    this.operationData = {};
  }

  handleOperationSelection() {
    this.operationData = {};
  }

  async handleNext(valid: boolean) {
    if (valid && this.productType) {
      this.operationData.source = { key: "ONLINE" };
      const payload: { step: string; data: Step13State } = {
        step: 'step13',
        data: {
          productType: this.productType,
          selectedOperation: this.selectedOperation,
          selectedProduct: this.selectedProduct,
          operationData: this.operationData
        }
      };
      try {
        if (this.$route.query && this.$route.query.operation && this.$route.query.operation !== 'add') {
          payload.data.id = +this.$route.query.operation - 1;
          await this.$store.dispatch('applicationStore/setOperationById', payload);
          this.$router.push('/paso14');
        } else {
          await this.$store.dispatch('applicationStore/setOperation', payload.data);
          await this.$store.dispatch('applicationStore/setDataStep', payload);
          this.updateOnboardingState(this.lead_uuid, 13);
          this.$router.push('/paso14');
        }
      } catch (err) {
        console.error(err);
      }
    }
  }

  get isPPDisabled(): boolean {
    const holders: PhysicalPersonData[] = this.interveners.filter(intervener => intervener.roles && intervener.roles.some(role => role.key === "TITULAR"));
    if (this.isJuridicalEntity) {
      this.PPDisabledReason = "Las entidades jurídicas no pueden realizar operaciones de plan de pensiones";
      return true;
    }
    if (holders.length > 1) {
      this.PPDisabledReason = "Las cuentas con múltiples titulares no pueden realizar operaciones de plan de pensiones";
      return true;
    }
    if (holders[0].nationality.key !== "ESP") {
      this.PPDisabledReason = "Solo las cuentas cuyo titular posea nacionalidad española pueden realizar operaciones de plan de pensiones";
      return true;
    }
    if (holders.some(holder => moment().diff(holder.birthdate, "years") < 18)) {
      this.PPDisabledReason = "Las cuentas con un titular menor de edad no pueden realizar operaciones de plan de pensiones";
      return true;
    }
    return false;
  }

  checkIfEditOperation() {
    // Si tenemos por query el parámetro 'operation' significa que desde la pantalla de
    // resumen el usuario quiere editar una operación.
    // Nota: Se le resta -1 para acceder al array de operaciones en el caso de que sea
    // en la posición 0.
    if (this.$route.query && this.$route.query.operation) {
      const { operations } = this.$store.getters['applicationStore/applicationData'];
      const operation = operations[+this.$route.query.operation - 1];
      if (operation) {
        this.productType = operation.productType;
        this.selectedOperation = operation.selectedOperation;
        this.selectedProduct = operation.selectedProduct;
        this.operationData = operation.operationData;
        this.loadOperationsOptions(this.productType);
        this.loadProductsOptions(this.productType);
      }
    }
  }
}
