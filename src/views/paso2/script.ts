/* eslint-disable  @typescript-eslint/no-explicit-any */
import { Component, Mixins } from 'vue-property-decorator';
import StepsMixin from '@/mixins/steps';

@Component
export default class Step2 extends Mixins(StepsMixin) {
  rellenar = false;

  lead_uuid = '';
  name = '';
  lastname_1 = '';
  lastname_2 = '';
  email = '';
  mobile_phone = '';
  mobile_phone_prefix: any = '';
  f_address_postal_code = '';
  lopd = false;
  loading = false;
  agent = null;
  postal_codes: any = [];

  onChange(field: string, value: any) {
    if (field === 'mobile_phone_prefix') {
      value = value.phone_prefix;
    }
    const data = this.$store.getters['applicationStore/applicationData'];
    const dataStep = data.step2;
    dataStep[field] = value;
    const payload = {
      step: 'step2',
      data: dataStep
    };
    this.$store.dispatch('applicationStore/setDataStep', payload);
  }

  next(valid: boolean) {
    if (valid && !this.loading) {
      // Cambiamos el estado del botón
      this.loading = !this.loading;

      // Preparamos
      const payload = {
        name: this.name,
        email: this.email,
        mobile_phone_prefix: `${this.mobile_phone_prefix.phone_prefix}`,
        mobile_phone: this.mobile_phone,
        f_address_postal_code: this.f_address_postal_code,
        language: 'es'
      };
      this.$onboardingService
        .createLead(payload)
        .then((res: any) => {
          // Comprobamos la respuesta si ha dado error o no
          if (res.result.http_code !== 200) {
            // Toast de error
            this.$bvToast.toast(`${res.result.info}`, {
              title: `Error ${res.result.http_code}`,
              autoHideDelay: 10000,
              variant: 'danger'
            });
            // Cambiamos el estado del botón
            this.loading = false;
          } else {
            // const _self = this;
            this.lead_uuid = res.data.lead.uuid;
            localStorage.setItem('LU', this.lead_uuid);
            // Guardamos en UUID del lead en store
            this.$store
              .dispatch('applicationStore/setLeadUuid', this.lead_uuid)
              .then(() => {
                // Enviamos email al usuario
                this.$onboardingService.sendEmail(this.email, this.lead_uuid).then(() => {
                  // Cambiamos el estado del botón
                  this.loading = false;
                });
              })
              .then(() => {
                this.updateOnboardingState(this.lead_uuid, 2);
              });
          }
        })
        .catch((err: Error) => {
          this.loading = false;
          this.$bvToast.toast(`${err.message}`, {
            title: `Error`,
            autoHideDelay: 10000,
            variant: 'danger'
          });
        });
    }
  }

  getAgent() {
    if (this.f_address_postal_code) {
      this.$onboardingService.getAgent(this.f_address_postal_code).then((res: any) => {
        this.agent = res.data.agent;
        // Guardamos el agente en el store
        this.$store.dispatch('applicationStore/setAgent', this.agent);
      });
    }
  }

  compare(a: any, b: any) {
    // Use toUpperCase() to ignore character casing
    const countryA = a.phone_prefix.toUpperCase();
    const countryB = b.phone_prefix.toUpperCase();

    let comparison = 0;
    if (countryA > countryB) {
      comparison = 1;
    } else if (countryA < countryB) {
      comparison = -1;
    }
    return comparison;
  }

  customPrefixCountry({ phone_prefix, name }: any) {
    return `${phone_prefix} — [${name}]`;
  }

  mounted() {
    // Obtenemos la información del store...
    const data = this.$store.getters['applicationStore/applicationData'];
    // ...y rellenamos el formulario
    this.name = data.step2.name;
    this.lastname_1 = data.step2.lastname_1;
    this.lastname_2 = data.step2.lastname_2;
    this.email = data.step2.email;
    this.mobile_phone_prefix = data.step2.mobile_phone_prefix;
    this.mobile_phone = data.step2.mobile_phone;
    this.f_address_postal_code = data.step2.f_address_postal_code;
    this.lopd = data.step2.lopd;

    this.$onboardingService.getCountries().then((res: any) => {
      const countriesList = res.countries_info;
      const i18nCountries: any = this.$t(`countries`);
      this.postal_codes = countriesList.map((x: any) => {
        return { name: i18nCountries[x.country_key], key: x.country_key, phone_prefix: x.phone_prefix };
      });
      this.postal_codes = this.postal_codes.filter((x: any) => x.phone_prefix && x.phone_prefix !== '+34');
      this.postal_codes.sort(this.compare);
      const spainCode: any = [
        {
          key: 'ES',
          name: 'España',
          phone_prefix: '+34'
        }
      ];
      this.postal_codes = [...spainCode, ...this.postal_codes];
    });
  }
}
