import { Component, Vue } from 'vue-property-decorator';
import { AxiosResponse } from 'axios';
import router from '@/router';
import OnboardingState from '@/interfaces/OnboardingState';

@Component({})
export default class DigitalError extends Vue {
    lead_uuid = '';
    video_identification_url = '';
    redirecting = false;

    created() {
        const data = this.$store.getters['applicationStore/applicationData'];
        this.lead_uuid = data.lead_uuid;
        this.$onboardingService.getVideoIdentificationUrl(this.lead_uuid)
            .then((res: AxiosResponse) => this.video_identification_url = res.data.video_identification_url)
            .catch((err: Error) => console.error(err));
    }

    handleActivation() {
        if (this.video_identification_url !== '') {
            window.location.href = this.video_identification_url;
        } else {
            this.redirecting = true;
            const checkUrlIsReady = setInterval(() => {
                if (this.video_identification_url !== '') {
                    clearInterval(checkUrlIsReady);
                    window.location.href = this.video_identification_url;
                }
            }, 1000);
        }
    }

    handleManualEntry() {
        const payload = {
            step: 'step5',
            data: { type: 'Manual' }
        };
        this.$store.dispatch('applicationStore/setDataStep', payload)
            .then(() => {
                const data = this.$store.getters['applicationStore/applicationData'];
                const newOnboardingState = { state_id: data.lead_uuid, actual_step: 5, state_json: data } as OnboardingState;
                this.$onboardingService.updateOnboardingState(newOnboardingState)
                    .then(() => router.push('/paso6'))
                    .catch((err: Error) => console.error(err));
            })
            .catch(err => console.error(err));
    }
}