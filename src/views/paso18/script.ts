/* eslint-disable  @typescript-eslint/no-explicit-any */
import { Component, Mixins } from 'vue-property-decorator';
import StepsMixin from '@/mixins/steps';

@Component
export default class Step18 extends Mixins(StepsMixin) {
  lead_uuid = '';

  created() {
    console.log('created step 18');
  }
}
