/* eslint-disable  @typescript-eslint/no-non-null-assertion */
import { Component, Vue } from 'vue-property-decorator';
import OnboardingState from '@/interfaces/OnboardingState';
import router from '@/router';
import OnboardingStateBody from '@/interfaces/OnboardingStateBody';

@Component({})
export default class Step9 extends Vue {
    lead_uuid = '';
    gdpr_1 = false;
    gdpr_2 = false;
    gdpr_3 = false;
    gdpr_4 = false;
    reject_contact = false;

    get showIntervenerAlert() {
        const { isJuridicalEntity, settedIsJuridicalEntity, interveners } = this.$store.getters['applicationStore/applicationData'] as OnboardingStateBody;
        const nonKycInterveners = interveners.filter(intervener => !intervener.kyc);
        return isJuridicalEntity && settedIsJuridicalEntity && nonKycInterveners.length < 2;
    }

    created() {
        const data = this.$store.getters['applicationStore/applicationData'];
        this.lead_uuid = data.lead_uuid;
    }

    handleNext() {
        if (this.gdpr_1 && this.gdpr_2 && this.gdpr_3 && this.gdpr_4) {
            const payload = {
                step: 'step9',
                data: { gdpr_1: this.gdpr_1, gdpr_2: this.gdpr_2, gdpr_3: this.gdpr_3, gdpr_4: this.gdpr_4, reject_contact: this.reject_contact }
            };
            this.$store.dispatch('applicationStore/setDataStep', payload)
                .then(() => {
                    const data = this.$store.getters['applicationStore/applicationData'];
                    const newOnboardingState = { state_id: this.lead_uuid, actual_step: 9, state_json: data } as OnboardingState;
                    const { isJuridicalEntity } = this.$store.getters['applicationStore/applicationData'];
                    if (isJuridicalEntity === true) {
                        router.push('/paso5');
                    } else {
                        router.push('/paso10');
                    }
                    this.$onboardingService.updateOnboardingState(newOnboardingState)
                        .catch((err: Error) => console.error(err));
                })
                .catch(err => console.error(err));
        }
    }
}