/* eslint-disable  @typescript-eslint/no-explicit-any */
import { Component, Mixins } from 'vue-property-decorator';
import AddressForm from './address-form/index.vue';
import Address from '@/interfaces/Address';
import OnboardingStateBody from '@/interfaces/OnboardingStateBody';
import StepsMixin from '@/mixins/steps';

@Component({
  components: {
    AddressForm
  }
})
export default class Step7 extends Mixins(StepsMixin) {
  lead_uuid = '';
  isJuridicalEntity = false;
  addPostalAddress = false;
  valid = false;
  fiscalAddress: Address = {
    street_type: {},
    street_1: '',
    number: '',
    floor: '',
    door: '',
    postal_code: '',
    city: '',
    state: '',
    key_country: ''
  };
  postalAddress: Address = {
    street_type: {},
    street_1: '',
    number: '',
    floor: '',
    door: '',
    postal_code: '',
    city: '',
    state: '',
    key_country: ''
  };
  streetTypesOptions: any[] = [];
  provincesOptions: any[] = [];
  provincesLoaded = false;
  loading = false;

  created() {
    const data: OnboardingStateBody = this.$store.getters['applicationStore/applicationData'];
    this.lead_uuid = data.lead_uuid;
    this.isJuridicalEntity = data.isJuridicalEntity;
    this.loadStreetTypesOptions();
    if (this.$route.query.intervener) {
      this.loadProvincesOptions(data.interveners[+this.$route.query.intervener - 1].country.key);
    }
    else {
      this.loadProvincesOptions(data.interveners[data.interveners.length - 1].country.key);
    }
  }

  loadStreetTypesOptions() {
    const streetTypes = this.$t(`streetTypes`);
    this.streetTypesOptions = Object.entries(streetTypes).map(item => {
      return {
        key: item[0],
        name: item[1]
      };
    });
  }

  loadProvincesOptions(countryKey: string) {
    this.$onboardingService.getProvinces(countryKey).then((res: any) => {
      this.provincesOptions = res.data.provinces.map((x: any) => {
        return { name: x.name, key: x.key_name };
      });
      this.provincesLoaded = true;
    });
  }

  getLeadData(addressesData: any, stateData: OnboardingStateBody) {
    if (this.isJuridicalEntity) {
      return {
        social_reason: stateData.step6.lastname_1,
        document_id: stateData.step6.document_number,
        birthdate: stateData.step6.birthdate,
        email: stateData.step6.email,
        home_phone_prefix: stateData.step6.prefix,
        home_phone: stateData.step6.mobile_phone,
        prefered_comunication_mode: stateData.step6.contact_type,
        ...addressesData
      };
    }
    return {
      name: stateData.interveners[0].name,
      lastname_1: stateData.interveners[0].lastname_1,
      lastname_2: stateData.interveners[0].lastname_2,
      document_type: stateData.interveners[0].document_type,
      document_id: stateData.interveners[0].document_number,
      document_expiration_date: stateData.interveners[0].document_date,
      birthdate: stateData.interveners[0].birthdate,
      key_country_of_birth: stateData.interveners[0].place_of_birth.key,
      key_nationality: stateData.interveners[0].nationality.key,
      key_country: stateData.interveners[0].country.key,
      marital_status: stateData.interveners[0].marital_status.key,
      gender: stateData.interveners[0].gender,
      ...addressesData
    };
  }

  handleError(error: Error) {
    this.loading = false;
    console.error(error);
    this.$bvToast.toast(`${error.message}`, {
      title: `Error`,
      autoHideDelay: 10000,
      variant: 'danger'
    });
  }

  async handleNext() {
    if (this.$route.query && this.$route.query.intervener) {
      // Actualizar el lead con los datos nuevos del intervener
      this.$router.push('/paso14');

    } else {
      const addressesData: any = {
        fiscal_address: { ...this.fiscalAddress },
        postal_address: this.addPostalAddress ? { ...this.postalAddress } : { ...this.fiscalAddress }
      };
      const stateData: OnboardingStateBody = this.$store.getters['applicationStore/applicationData'];
      const leadData = this.getLeadData(addressesData, stateData);
      this.loading = true;

      try {
        if (this.isLegalIntervener) {
          await this.$store.dispatch('applicationStore/setIsJuridicalEntity', {
            isJuridicalEntity: stateData.isJuridicalEntity,
            settedIsJuridicalEntity: stateData.settedIsJuridicalEntity,
            createInterveners: true
          });
        }
        await this.$onboardingService.createLeadPersonTypeData(leadData, this.isJuridicalEntity ? 'juridical' : 'physical', this.lead_uuid);
        await this.$store.dispatch('applicationStore/setDataStep', { step: 'step7', addressesData });
        await this.$store.dispatch('applicationStore/setAddressToIntervener', { idIntervener: stateData.interveners.length - 1, addresses: addressesData });
        if (stateData.interveners.length > 1) {
          this.$router.push('/paso11'); // Listado Añadir intervinientes
        } else {
          this.$router.push('/paso8'); // Test de conocimiento
        }
        this.loading = false;
        this.updateOnboardingState(this.lead_uuid, stateData.interveners.length > 1 ? 10 : 7);

      } catch (err) {
        this.handleError(err);
      }
    }
  }

  // booleano que representa si se está creando el interviniente de la propia persona jurídica
  get isLegalIntervener() {
    const { isJuridicalEntity, settedIsJuridicalEntity, interveners } = this.$store.getters[
      'applicationStore/applicationData'
    ];
    return isJuridicalEntity && settedIsJuridicalEntity && interveners.length < 2;
  }

  mounted() {
    // Si tenemos por query el parámetro 'intervener' significa que desde la pantalla de
    // resumen el usuario quiere editar un interviniente.
    // Nota: Se le resta -1 para acceder al array de intervinientes en el caso de que sea
    // en la posición 0.
    if (this.$route.query && this.$route.query.intervener) {
      const { interveners } = this.$store.getters['applicationStore/applicationData'];
      const intervener = interveners[+this.$route.query.intervener - 1];

      this.fiscalAddress = intervener.addresses.fiscal_address;
      this.postalAddress = intervener.addresses.postal_address;

      // Se eliminan las provincias seleccionadas anteriormente, puesto que si se ha cambiado el país,
      // dejan de ser válidas
      this.fiscalAddress.state = null;
      this.postalAddress.state = null;

      if (JSON.stringify(this.fiscalAddress) === JSON.stringify(this.postalAddress)) {
        this.addPostalAddress = false;
      }
      else {
        this.addPostalAddress = true;
      }
    }
  }
}
