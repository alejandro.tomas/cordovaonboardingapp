/* eslint-disable  @typescript-eslint/no-explicit-any */
import { Component, Vue, Prop } from 'vue-property-decorator';
import Address from '@/interfaces/Address';

@Component({})
export default class AddressForm extends Vue {
    @Prop({ required: true })
    address!: Address;
    @Prop({ required: true })
    streetTypesOptions!: any;
    @Prop({ required: true })
    provincesOptions!: any;
    @Prop({ required: true })
    provincesLoaded!: boolean;

    mounted() {
        // if (process.env.VUE_APP_ENV === 'development') {
        //     const numRandom = Math.floor(Math.random() * 10) + 1;
        //     this.$onboardingService.getUrl(`https://jsonplaceholder.typicode.com/users/${numRandom}`).then((user: any) => {
        //         console.log(user);
        //         const name = user.name.split(' ');
        //         this.address.street_type = { key: "CL", name: "Calle" };
        //         this.address.street_1 = user.address.street;
        //         this.address.number = '1';
        //         this.address.floor = '2';
        //         this.address.door = '3F';
        //         this.address.postal_code = '12001';
        //         this.address.city = user.address.city;
        //         setTimeout(() => {
        //             this.address.state = { name: "Cáceres", key: "ES-CC" };
        //         }, 1000);
        //     });
        // }
    }

}