/* eslint-disable  @typescript-eslint/no-explicit-any */
/* eslint-disable  @typescript-eslint/no-non-null-assertion */
import { Component, Mixins } from 'vue-property-decorator';
import StepsMixin from '@/mixins/steps';
import OnboardingStateBody from '@/interfaces/OnboardingStateBody';
import PhysicalPersonData from '@/interfaces/PhysicalPersonData';
import Step12State from '@/interfaces/Step12State';
import Step14State from '@/interfaces/Step14State';

@Component
export default class Step16 extends Mixins(StepsMixin) {
  lead_uuid = '';
  interveners: PhysicalPersonData[] = [];
  isJuridicalEntity = false;
  signatureType = '';
  step8Responses: any[] = [];
  step12?: Step12State = undefined;
  step14?: Step14State = undefined;
  operationsType = '';

  accountDocuments: any;
  intervenersDocuments: any = [];
  operationsDocuments: any = [];
  loading = true;

  created() {
    const data: OnboardingStateBody = this.$store.getters['applicationStore/applicationData'];
    this.lead_uuid = data.lead_uuid;
    this.interveners = data.interveners;
    this.isJuridicalEntity = data.isJuridicalEntity;
    this.signatureType = data.step11.signatureType;
    this.step8Responses = data.step8;
    this.step12 = data.step12;
    this.step14 = data.step14;
    this.operationsType = data.operationsType;
    // this.loadTickets();
    this.loadDocuments();
  }

  async loadDocuments() {
    const { account, step14 } = this.$store.getters['applicationStore/applicationData'];

    // Obtenemos los documentos de la cuenta
    await this.$onboardingService
      .getAccountDocumentsValidation(account.uuid)
      .then((res: any) => {
        this.accountDocuments = res.data.accounts_documents.filter((x: any) => x.uuid);
      })
      .catch((err: Error) => {
        console.error(err);
      });

    // Obtenemos los intervinientes de una cuenta
    let interveners: any = [];
    await this.$onboardingService
      .getIntervenersAccount(account.uuid)
      .then((res: any) => (interveners = res.data.accounts_intervener))
      .catch((err: Error) => {
        console.error(err);
      });

    // Obtenemos los documentos de cada interviniente
    const endpointsInterveners: any = [];
    interveners.forEach((intervener: any) => {
      endpointsInterveners.push(this.$onboardingService.getDocumentsValidation(intervener.uuid));
    });
    await Promise.all(endpointsInterveners)
      .then((res: any) => {
        let indexIntervener = 0;
        res.forEach((result: any) => {
          this.intervenersDocuments.push({
            intervener: interveners[indexIntervener],
            documents: result.data.interveners_documents.filter((x: any) => x.uuid)
          });
          indexIntervener++;
        });
      })
      .catch((err: Error) => {
        console.error(err);
      });

    // Obtenemos los documentos de cada operación
    const endpointsOperations: any = [];
    step14.operationsCreated.forEach((operation: any) => {
      endpointsOperations.push(this.$onboardingService.getDocumentsOperations(operation.operation_id));
    });
    await Promise.all(endpointsOperations)
      .then((res: any) => {
        let indexOperation = 0;
        res.forEach((result: any) => {
          this.operationsDocuments.push({
            operation: step14.operationsCreated[indexOperation],
            documents: result.data.operation_documents.filter((x: any) => x.uuid)
          });
          indexOperation++;
        });
      })
      .catch((err: Error) => {
        console.error(err);
      });
    this.loading = false;
  }

  loadTickets() {
    const intervenersTicketData: any = this.formatIntervenersTicketData();
    const knowledgeTestTicketData: any = this.isJuridicalEntity ? this.formatLegalKnowledgeTestTicketData() : this.formatPhysicalKnowledgeTestTicketData();
    const convenienceTestTicketData: any = this.formatConvenienceTestTicketData();
    const fatcaAndCrsTicketData: any = this.formatFatcaAndCrsTicketData();
    const thirdPartyAuthorizationTicketData: any = this.formatThirdPartyAuthorizationTicketData();
    const convenientOrNotTicketsData: any[] = this.formatConvenientOrNotTicketsData();

    const documentsPromises = this.$onboardingService.generateDocuments([
      intervenersTicketData,
      knowledgeTestTicketData,
      convenienceTestTicketData,
      fatcaAndCrsTicketData,
      thirdPartyAuthorizationTicketData,
      ...convenientOrNotTicketsData
    ]);
    Promise.all(documentsPromises)
      .then(res => console.log(res))
      .catch(err => console.error(err));
  }

  handleNext() {
    console.log('Next!');
    this.$router.push('paso17');
  }

  handleDownloadDocument(documentData: any, type: string) {
    let documentDownloadCall: Promise<any>;
    if (type === "interveners") {
      documentDownloadCall = this.$onboardingService.downloadIntervenerDocument(documentData.uuid_intervener, documentData.uuid);
    } else if (type === "accounts") {
      documentDownloadCall = this.$onboardingService.downloadAccountDocument(documentData.uuid_account, documentData.uuid);
    } else {
      documentDownloadCall = this.$onboardingService.downloadOperationDocument(documentData.uuid_operation, documentData.uuid);
    }
    documentDownloadCall
      .then(documentBytes => {
        const blob = new Blob([documentBytes], { type: "application/pdf" });
        const link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.download = documentData.title;
        link.click();
      })
      .catch(err => console.error(err));
  }

  formatIntervenersTicketData(): any {
    const fiscal_address = this.interveners[0].addresses.fiscal_address;
    const postal_address = this.interveners[0].addresses.postal_address;
    return {
      s3_bucket: "uploads-pre",
      s3_folder: this.step14?.accountCreated.id_object,
      s3_name: `ALTA-CLIENTE_${this.step14?.accountCreated.id_object}_${new Date().getTime()}.pdf`,
      object_id: this.step14?.accountCreated.fiscal_address.intervener.id_object,
      language: "es",
      key_operation_type: "ALTA_CLIENTE",
      signatureType: this.signatureType,
      communications: this.interveners[0].comunicationType,
      // todo
      bank_account: {},
      fiscal_address: {
        ...fiscal_address,
        street: `${fiscal_address?.street_type} ${fiscal_address?.street_1}, ${fiscal_address?.number}, ${fiscal_address?.floor}.º ${fiscal_address?.door}`,
        country: this.$t(`countries.${fiscal_address?.key_country}`)
      },
      postal_address: {
        ...postal_address,
        street: `${postal_address?.street_type} ${postal_address?.street_1}, ${postal_address?.number}, ${postal_address?.floor}.º ${postal_address?.door}`,
        country: this.$t(`countries.${postal_address?.key_country}`)
      },
      interveners: this.formatIntervenersData(this.holders),
      tutors: this.formatIntervenersData(this.tutorsRepresentativesAndAuthorized),
      // TODO: ESTO SERÍAN LOS DATOS DE LA FIRMA DIGITAL
      signature: {}
    }
  }

  formatIntervenersData(interveners: PhysicalPersonData[]): any {
    return interveners.map(intervener => {
      return {
        intervener_type: intervener.intervener_type,
        full_name: `${intervener.name} ${intervener.lastname_1} ${intervener.lastname_2}`,
        birthdate: intervener.birthdate,
        place_of_birth: intervener.place_of_birth,
        gender: intervener.gender,
        document_type: intervener.document_type,
        document_number: intervener.document_number,
        nationality: intervener.nationality,
        country: intervener.country,
        marital_status: intervener.marital_status,
        roles: intervener.roles,
        mobile_number: intervener.mobile_phone
      }
    });
  }

  formatLegalKnowledgeTestTicketData(): any {
    // el interviniente principal será aquel que sea de tipo físico y tenga el rol titular o nudo propietario (si hay varios se utiliza el primero)
    const mainIntervener = this.interveners.find(intervener =>
      intervener.intervener_type === "F" && intervener.roles?.some(role => role.key === "TITULAR" || role.key === "NUDO_PROPIETARIO"));
    return {
      s3_bucket: "uploads-pre",
      s3_folder: this.step14?.accountCreated.id_object,
      s3_name: `CONOCIMIENTO-JURIDICA_${this.step14?.accountCreated.id_object}_${new Date().getTime()}.pdf`,
      object_id: this.step14?.accountCreated.fiscal_address.intervener.id_object,
      language: "es",
      key_operation_type: "CONOCIMIENTO_JURIDICA",
      knowledge_test: this.step8Responses,
      real_holders_and_admins: this.realHoldersAndAdmins,
      trustees: this.interveners.filter(intervener => intervener.roles?.some(role => role.key === "PATRONO")),
      // TODO: ESTO SERÍAN LOS DATOS DE LA FIRMA DIGITAL
      signature: {},
      main_intervener: {
        full_name: `${mainIntervener?.name} ${mainIntervener?.lastname_1} ${mainIntervener?.lastname_2}`,
        role: mainIntervener?.roles?.some(role => role.key === "TITULAR") ? "Titular" : "Nudo propietario",
        company: this.interveners[0].lastname_1,
        document_id: mainIntervener?.document_number
      }
    }
  }

  formatPhysicalKnowledgeTestTicketData(): any {
    // el interviniente principal será aquel que tenga el rol titular o nudo propietario (si hay varios se utiliza el primero)
    const mainIntervener = this.interveners.find(intervener => intervener.roles?.some(role => role.key === "TITULAR" || role.key === "NUDO_PROPIETARIO"));
    return {
      s3_bucket: "uploads-pre",
      s3_folder: this.step14?.accountCreated.id_object,
      s3_name: `CONOCIMIENTO-FISICA_${this.step14?.accountCreated.id_object}_${new Date().getTime()}.pdf`,
      object_id: this.step14?.accountCreated.fiscal_address.intervener.id_object,
      language: "es",
      key_operation_type: "CONOCIMIENTO_FISICA",
      knowledge_test: this.step8Responses,
      // TODO: ESTO SERÍAN LOS DATOS DE LA FIRMA DIGITAL
      signature: {},
      main_intervener: {
        full_name: `${mainIntervener?.name} ${mainIntervener?.lastname_1} ${mainIntervener?.lastname_2}`,
        role: mainIntervener?.roles?.some(role => role.key === "TITULAR") ? "Titular" : "Nudo propietario"
      }
    }
  }

  formatConvenienceTestTicketData(): any {
    return {
      s3_bucket: "uploads-pre",
      s3_folder: this.step14?.accountCreated.id_object,
      s3_name: `CONVENIENCIA_${this.step14?.accountCreated.id_object}_${new Date().getTime()}.pdf`,
      object_id: this.step14?.accountCreated.fiscal_address.intervener.id_object,
      language: "es",
      key_operation_type: "CONVENIENCIA",
      knowledgeTest: this.step12?.responses,
      // TODO: ESTO SERÍAN LOS DATOS DE LA FIRMA DIGITAL
      signature: {}
    }
  }

  formatFatcaAndCrsTicketData(): any {
    return {
      s3_bucket: "uploads-pre",
      s3_folder: this.step14?.accountCreated.id_object,
      s3_name: `FATCA_${this.step14?.accountCreated.id_object}_${new Date().getTime()}.pdf`,
      object_id: this.step14?.accountCreated.fiscal_address.intervener.id_object,
      language: "es",
      key_operation_type: "FATCA",
      fatca: {
        selected: this.isJuridicalEntity
          ? this.step8Responses[10].answer[0] === 'SI'
          : this.step8Responses[12].answer[0] === 'SI',
        number: this.isJuridicalEntity ? this.step8Responses[10].answer[1] : this.step8Responses[12].answer[1],
        legal_entity_type: this.isJuridicalEntity ? this.step8Responses[11].answer[1] : '',
        custom_legal_entity_type: this.isJuridicalEntity ? this.step8Responses[12].answer[1] : ''
      },
      crs: {
        selected: this.isJuridicalEntity
          ? this.step8Responses[9].answer[0] === 'SI'
          : this.step8Responses[11].answer[0] === 'SI',
        country: this.isJuridicalEntity ? this.step8Responses[9].answer[1] : this.step8Responses[11].answer[1],
        number: this.isJuridicalEntity ? this.step8Responses[9].answer[2] : this.step8Responses[11].answer[2],
      },
      // TODO: ESTO SERÍAN LOS DATOS DE LA FIRMA DIGITAL
      signature: {}
    }
  }

  formatThirdPartyAuthorizationTicketData(): any {
    return {
      s3_bucket: "uploads-pre",
      s3_folder: this.step14?.accountCreated.id_object,
      s3_name: `AUTORIZACION_TERCEROS_${this.step14?.accountCreated.id_object}_${new Date().getTime()}.pdf`,
      object_id: this.step14?.accountCreated.fiscal_address.intervener.id_object,
      language: "es",
      key_operation_type: "AUTORIZACION_TERCEROS",
      holders: this.interveners
        .filter(intervener => intervener.roles?.some(role => role.key === "TITULAR"))
        .map(intervener => {
          return {
            full_name: `${intervener.name} ${intervener.lastname_1} ${intervener.lastname_2}`,
            document_type: intervener.document_type,
            name: intervener.name,
            lastname_1: intervener.lastname_1,
            lastname_2: intervener.lastname_2,
            document_id: intervener.document_number
          }
        }),
      authorized: this.interveners
        .filter(intervener => intervener.roles?.some(role => role.key.includes("AUTORIZADO")))
        .map(intervener => {
          return {
            full_name: `${intervener.name} ${intervener.lastname_1} ${intervener.lastname_2}`,
            document_type: intervener.document_type,
            name: intervener.name,
            lastname_1: intervener.lastname_1,
            lastname_2: intervener.lastname_2,
            document_id: intervener.document_number
          }
        }),
      signature: {
        date: new Date(),
        place: "ONLINE"
      },
      // TODO: adaptar para mandar si se ha utilizado firma digital o no
      is_digital_signature: false
    }
  }

  formatConvenientOrNotTicketsData(): any[] {
    return this.step14!.operationsCreated.map(operation => {
      let key_operation_type = "CONVENIENTE";
      // si no es conveniente se utiliza NO_CONVENIENTE_FIL para concentrados y NO_CONVENIENTE para el resto
      if (this.isOperationNotConvenient(operation)) {
        key_operation_type = operation.key_product === "COBAS_CONCENTRADOS_FIL" ? "NO_CONVENIENTE_FIL" : "NO_CONVENIENTE";
      }
      return {
        s3_bucket: "uploads-pre",
        s3_folder: this.step14?.accountCreated.id_object,
        s3_name: `${key_operation_type.split('_').join('-')}_${this.step14?.accountCreated.id_object}_${new Date().getTime()}.pdf`,
        id_object: operation.id_object,
        language: "es",
        key_operation_type: key_operation_type,
        operation_name: `${operation.key_operation_type} ${operation.key_product.split('_').join(' ')}`,
        operation_date: new Date(),
        key_product: operation.key_product,
        // TODO: ESTO SERÍAN LOS DATOS DE LA FIRMA DIGITAL
        signature: {}
      }
    })
  }

  isOperationNotConvenient(operation: any): boolean {
    if (operation.key_product === 'COBAS_RENTA') {
      return (
        this.step12?.responses[0].answer === 'CONVENIENCE_Q1_A1' &&
        this.step12?.responses[1].answer === 'CONVENIENCE_Q2_A1'
      );
    }
    return this.step12!.score < 0;
  }

  get holders(): PhysicalPersonData[] {
    return this.interveners.filter(intervener => intervener.roles && intervener.roles.some(role => role.key === "TITULAR"));
  }

  get realHoldersAndAdmins(): PhysicalPersonData[] {
    const filteredRoles = ["TITULAR_REAL", "ADMINISTRADOR"];
    return this.interveners.filter(intervener => intervener.roles && intervener.roles.some(role => filteredRoles.includes(role.key)));
  }

  get tutorsRepresentativesAndAuthorized(): PhysicalPersonData[] {
    const filteredRoles = ["TUTOR", "AUTORIZADO", "AUTORIZADO_CONSULTA", "REPRESENTANTE_LEGAL"];
    return this.interveners.filter(intervener => intervener.roles && intervener.roles.some(role => filteredRoles.includes(role.key)));
  }
}
