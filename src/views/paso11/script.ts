/* eslint-disable  @typescript-eslint/no-non-null-assertion */
import { Component, Mixins } from 'vue-property-decorator';
import RowIntervener from './row-intervener/index.vue';
import Step11State from '@/interfaces/Step11State';
import moment from "moment";
import OnboardingStateBody from '@/interfaces/OnboardingStateBody';
import { AxiosResponse } from 'axios';
import PhysicalPersonData from '@/interfaces/PhysicalPersonData';
import StepsMixin from '@/mixins/steps';

@Component({
  components: {
    RowIntervener
  }
})
export default class Step11 extends Mixins(StepsMixin) {
  lead_uuid = '';
  loading = false;
  interveners: PhysicalPersonData[] = [];
  mainRoles: any[] = [];
  secondaryRoles: any[] = [];
  signatureType = '';
  isJuridicalEntity = false;
  addIntervener = false;
  addSecondaryIntervener = false;
  showLogin = true;
  username = '';
  password = '';
  PPDisabledReason = '';
  operationsType = '';

  created() {
    const data: OnboardingStateBody = this.$store.getters['applicationStore/applicationData'];
    this.lead_uuid = data.lead_uuid;
    this.interveners = data.interveners;
    this.isJuridicalEntity = data.isJuridicalEntity;
    this.checkIfEditInterveners();
    if (data.operationsType) {
      this.operationsType = data.operationsType;
      this.loadRoles(data.operationsType);
    }
    if (!this.isJuridicalEntity || this.interveners[0].document_number.charAt(0) !== "G") {
      this.secondaryRoles = this.secondaryRoles.filter(role => (role.key !== "PATRONO") && (role.key !== "JUNTA_DIRECTIVA"));
    }
    if (data.isAddingSecondaryIntervener) {
      this.$store.dispatch('applicationStore/setAddingSecondaryIntervener', false);
    }
  }

  loadRoles(productType: string) {
    const roles = this.$t(`intervenersStats.${this.isJuridicalEntity ? "legal" : "physical"}.${productType}`);
    const rolesOptions = Object.entries(roles).map(item => {
      return {
        key: item[0],
        name: item[1],
        $isDisabled: this.isJuridicalEntity && item[0] === "TITULAR"
      };
    });
    const mainRolesKeys = this.isJuridicalEntity ? ["TITULAR"] : ["TITULAR", "NUDO_PROPIETARIO"];
    this.mainRoles = rolesOptions.filter(role => mainRolesKeys.includes(role.key));
    this.secondaryRoles = rolesOptions.filter(role => !mainRolesKeys.includes(role.key));
  }

  handleOperationsTypeSelection(selectedType: string) {
    this.operationsType = selectedType;
    this.loadRoles(selectedType);
    this.$store.dispatch('applicationStore/setOperationsType', selectedType);
  }

  handleNext() {
    if (this.isFormValid) {
      const payload: { step: string; data: Step11State } = {
        step: 'step11',
        data: {
          interveners: this.interveners,
          signatureType: this.signatureType
        }
      };
      const data: OnboardingStateBody = this.$store.getters['applicationStore/applicationData'];
      this.$store.dispatch('applicationStore/setDataStep', payload);
      const isEditing = (this.$route.query && this.$route.query.edit) || data.isEditing;
      this.$store.dispatch('applicationStore/setEditing', false);
      this.updateOnboardingState(this.lead_uuid, isEditing ? 13 : 11);
    }
  }

  handleLogin(isValid: boolean) {
    if (isValid && !this.loading) {
      const isIntervenerAlreadyAdded = this.interveners.some(intervener => intervener.email === this.username);
      if (isIntervenerAlreadyAdded) {
        this.$bvToast.toast("Este usuario ya ha sido añadido", {
          title: `Error`,
          autoHideDelay: 10000,
          variant: 'danger'
        });
        return;
      }
      this.loading = true;
      this.$onboardingService.login({ username: this.username, password: this.password })
        .then((response: AxiosResponse) => {
          response.data.intervener.percent = 0;
          const formattedIntervener: PhysicalPersonData = this.formatLoginIntervener(response.data.intervener);
          this.$store.dispatch('applicationStore/setIntervener', formattedIntervener);
          this.addIntervener = false;
          this.addSecondaryIntervener = false;
          this.loading = false;
          this.updateOnboardingState(this.lead_uuid, 10);
        })
        .catch((err: Error) => {
          console.error(err);
          this.$bvToast.toast("Usuario o contraseña incorrectos", {
            title: `Error`,
            autoHideDelay: 10000,
            variant: 'danger'
          });
          this.loading = false;
        });
    }
  }

  formatLoginIntervener(loginIntervener: any): PhysicalPersonData {
    return {
      name: loginIntervener.name,
      lastname_1: loginIntervener.lastname_1,
      lastname_2: loginIntervener.lastname_2,
      intervener_type: loginIntervener.intervener_type,
      birthdate: loginIntervener.birthdate,
      gender: loginIntervener.gender,
      document_date: loginIntervener.document_expiration_date,
      document_type: { key: loginIntervener.document_type },
      document_number: loginIntervener.document_id,
      is_document_permanent: loginIntervener.is_document_permanent,
      email: loginIntervener.email,
      country: { key: loginIntervener.f_key_address_country },
      nationality: { key: loginIntervener.key_nationality_1 },
      place_of_birth: { key: loginIntervener.key_country_of_birth },
      marital_status: { key: loginIntervener.marital_status },
      secondary: this.addSecondaryIntervener,
      addresses: {
        fiscal_address: {
          city: loginIntervener.f_address_city,
          door: loginIntervener.f_address_door,
          floor: loginIntervener.f_address_floor,
          key_country: { key: loginIntervener.f_key_address_country },
          number: loginIntervener.f_address_number,
          postal_code: loginIntervener.f_address_postal_code,
          state: { key: loginIntervener.f_address_state },
          street_1: loginIntervener.f_address_street_1,
          street_type: { key: loginIntervener.f_address_street_type }
        },
        postal_address: {
          city: loginIntervener.city,
          door: loginIntervener.door,
          floor: loginIntervener.floor,
          key_country: { key: loginIntervener.key_country },
          number: loginIntervener.number,
          postal_code: loginIntervener.postal_code,
          state: { key: loginIntervener.key_state },
          street_1: loginIntervener.street_1,
          street_type: { key: loginIntervener.street_type }
        }
      }
    };
  }

  handleCancel() {
    this.addIntervener = false;
    this.addSecondaryIntervener = false;
    this.loading = false;
  }

  // comprueba si el interviniente tiene alguno de los roles
  hasIntervenerSomeRole(intervener: PhysicalPersonData, keys: string[]): boolean {
    return (intervener.roles && intervener.roles.some(role => keys.includes(role.key))) || false;
  }

  checkIfEditInterveners() {
    if (this.$route.query && this.$route.query.edit) {
      this.$store.dispatch('applicationStore/setEditing', true);
      this.updateOnboardingState(this.lead_uuid, 10);
    }
  }

  // guardamos los index para luego poder utilizarlos al borrar intervinientes desde las dos tablas
  get indexedInterveners(): PhysicalPersonData[] {
    return this.interveners.map((intervener, index) => {
      intervener.originalIndex = index;
      return intervener;
    });
  }

  get mainInterveners(): PhysicalPersonData[] {
    return this.indexedInterveners.filter(intervener => !intervener.secondary);
  }

  get secondaryInterveners(): PhysicalPersonData[] {
    return this.indexedInterveners.filter(intervener => intervener.secondary);
  }

  get filteredSecondaryRoles() {
    const isBareOwnerSelected = this.mainInterveners.some(intervener => this.hasIntervenerSomeRole(intervener, ["NUDO_PROPIETARIO"]));
    return isBareOwnerSelected ? this.secondaryRoles : this.secondaryRoles.filter(secondaryRole => secondaryRole.key !== "USUFRUCTUARIO");
  }

  get isPPDisabled(): boolean {
    if (this.isJuridicalEntity) {
      this.PPDisabledReason = "Las entidades jurídicas no pueden realizar operaciones de plan de pensiones";
      return true;
    }
    if (this.mainInterveners.length > 1) {
      this.PPDisabledReason = "Las cuentas con múltiples titulares no pueden realizar operaciones de plan de pensiones";
      return true;
    }
    if (this.mainInterveners.some(holder => holder.nationality.key !== "ESP")) {
      this.PPDisabledReason = "Solo las cuentas cuyo titular posea nacionalidad española pueden realizar operaciones de plan de pensiones";
      return true;
    }
    if (this.mainInterveners.some(holder => moment().diff(holder.birthdate, "years") < 18)) {
      this.PPDisabledReason = "Las cuentas con un titular menor de edad no pueden realizar operaciones de plan de pensiones";
      return true;
    }
    return false;
  }

  get isFormValid(): boolean {
    if (!this.signatureType) {
      return false;
    }
    if (this.signatureType === "MANCOMUNADA_ESPECIAL") {
      const isAnyIntervenerRequiredToSign = this.interveners.some(intervener => intervener.is_signature_required);
      if (!isAnyIntervenerRequiredToSign) {
        return false;
      }
    }
    if (this.warningMessage) {
      return false;
    }
    return true;
  }

  get warningMessage(): string {
    return this.juridicalWarning || this.physicalWarning || this.minorWarning
      || this.bareAndBeneficialOwnersWarning || this.foundationWarning || this.authorizedWarning || this.intervenersWarning || '';
  }

  get intervenersWarning() {
    const existsIntervenerWithoutRole = this.interveners.some(intervener => !intervener.roles || !intervener.roles.length);
    if (existsIntervenerWithoutRole) {
      return "Todos los intervinientes deben tener asignado al menos un rol";
    }
  }

  get juridicalWarning() {
    if (this.isJuridicalEntity) {
      const existsProxyOrAdmin = this.interveners.some(intervener => this.hasIntervenerSomeRole(intervener, ["APODERADO", "ADMINISTRADOR"]));
      if (!existsProxyOrAdmin) {
        return "Debe existir al menos un apoderado o administrador";
      }
    }
  }

  get physicalWarning() {
    if (!this.isJuridicalEntity) {
      const existsHolderOrBareOwner = this.interveners.some(intervener => this.hasIntervenerSomeRole(intervener, ["TITULAR", "NUDO_PROPIETARIO"]));
      if (!existsHolderOrBareOwner) {
        return "Debe existir al menos un titular o nudo propietario";
      }
    }
  }

  get bareAndBeneficialOwnersWarning() {
    const bareOwners = this.interveners.filter(intervener => this.hasIntervenerSomeRole(intervener, ["NUDO_PROPIETARIO"]));
    const beneficialOwners = this.interveners.filter(intervener => this.hasIntervenerSomeRole(intervener, ["USUFRUCTUARIO"]));
    const holders = this.interveners.filter(intervener => this.hasIntervenerSomeRole(intervener, ["TITULAR"]));
    if (bareOwners.length && !beneficialOwners.length) {
      return "Debe existir un usufructuario";
    }
    if (!bareOwners.length && beneficialOwners.length) {
      return "Debe existir al menos un nudo propietario";
    }
    if (beneficialOwners.length > 1) {
      return "Solo puede existir un usufructuario";
    }
    if (bareOwners.length && holders.length) {
      return "No pueden existir titulares y nudo propietarios";
    }
  }

  get foundationWarning() {
    // si es fundacion
    if (this.isJuridicalEntity && this.interveners[0].document_number.charAt(0) === "G") {
      const trustees = this.interveners.filter(intervener => this.hasIntervenerSomeRole(intervener, ["PATRONO"]));
      if (!trustees.length) {
        return "Debe existir al menos un patrono";
      }
    }
  }

  get minorWarning() {
    // si hay titulares menores
    const minorHolders = this.interveners.filter(intervener => this.hasIntervenerSomeRole(intervener, ["TITULAR"]) && (moment().diff(intervener.birthdate, "years") < 18));
    if (!this.isJuridicalEntity && minorHolders.length) {
      const existsGuardian = this.interveners.some(intervener => this.hasIntervenerSomeRole(intervener, ["TUTOR"]));
      if (!existsGuardian) {
        return "Debe existir al menos un tutor para cada titular menor de edad";
      }
    }

    // si hay menores no titulares
    const nonHolderMinors = this.interveners.filter(intervener =>
      (intervener.roles && intervener.roles.some(role => role.key !== "TITULAR")) && (moment().diff(intervener.birthdate, "years") < 18));

    if (!this.isJuridicalEntity && nonHolderMinors.length) {
      const intervenerFullName = `${nonHolderMinors[0].name} ${nonHolderMinors[0].lastname_1} ${nonHolderMinors[0].lastname_2}`;
      return `${intervenerFullName} no puede tener el rol "${nonHolderMinors[0].roles!.find(role => role.key !== "TITULAR").name}" ya que es menor de edad`;
    }
  }

  get authorizedWarning() {
    if (this.interveners.some(intervener => this.hasIntervenerSomeRole(intervener, ["AUTORIZADO_CONSULTA"]) && this.hasIntervenerSomeRole(intervener, ["AUTORIZADO"]))) {
      return "Un interviniente no puede ser autorizado consultivo y operativo";
    }
  }
}
