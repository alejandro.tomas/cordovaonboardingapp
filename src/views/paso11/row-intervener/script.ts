/* eslint-disable  @typescript-eslint/no-explicit-any */
import { Component, Prop, Mixins } from 'vue-property-decorator';
import StepsMixin from '@/mixins/steps';

@Component
export default class RowIntervener extends Mixins(StepsMixin) {
  @Prop({ required: true }) lead_uuid!: string;
  @Prop({ required: true }) intervener!: any;
  @Prop({ required: true }) interveners!: any[];
  @Prop({ required: true }) index!: number;
  @Prop({ required: true }) rolesOptions!: any[];
  @Prop({ required: true }) isJuridicalEntity!: boolean;
  @Prop({ required: true }) isSecondaryTable!: boolean;

  setRole(roles: any[]) {
    if (roles && roles.some(role => role.key === "TITULAR")) {
      this.intervener.percent = null;
    }
  }

  removeIntervener(index: number) {
    this.$store.dispatch('applicationStore/removeIntervener', index);
    this.updateOnboardingState(this.lead_uuid, 10);
    this.$bvModal.hide(`modal__${index}`);
    this.$bvToast.toast(`El interviniente ha sido eliminado del proceso de alta.`, {
      title: `Interviniente eliminado`
    });
  }
}
