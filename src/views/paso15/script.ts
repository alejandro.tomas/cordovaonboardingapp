/* eslint-disable  @typescript-eslint/no-explicit-any */
import { Component, Mixins } from 'vue-property-decorator';
import StepsMixin from '@/mixins/steps';
import RowIntervener from './row-intervener/index.vue';
import UploadDocument from './upload-document/index.vue';

@Component({
  components: {
    RowIntervener,
    UploadDocument
  }
})
export default class Step15 extends Mixins(StepsMixin) {
  lead_uuid = ''; // Lead actual
  interveners = []; // Array de intervinientes para la cuenta donde queremos subir documentos

  // Variables para subir documento
  showUploadSection = false; // Flag para saber si tenemos que mostrar la sección de upload
  showPreviewFile = false; // Flag para saber si tenemos que mostrar un archivo
  filePreview: any = null; // Archivo mostrado
  filePreviewLoading = false; // Archivo mostrado cargando
  fileType = null;
  numPages = null;
  intervener = null; // Interviniente para subir archivo
  document = null; // Información del documento para subir
  count = 1;

  created() {
    this.fetchData();
  }

  fetchData() {
    const { account } = this.$store.getters['applicationStore/applicationData'];
    this.$onboardingService
      .getIntervenersAccount(account.uuid)
      .then((res: any) => (this.interveners = res.data.accounts_intervener))
      .catch((err: Error) => console.error(err));
  }

  mounted() {
    // Obtenemos los ficheros de nuevo
    this.$root.$on('emit_fetch_data', () => {
      this.fetchData();
      this.count++;
    });

    // Mostrar upload de archivos
    this.$root.$on('emit_show_upload', (intervener: any, document: any) => {
      this.showUploadSection = true;
      this.showPreviewFile = false;
      this.intervener = intervener;
      this.document = document;
    });
    // Preview de archivo
    this.$root.$on('emit_preview_file', (intervener: any, document: any) => {
      this.showPreviewFile = true;
      this.showUploadSection = false;
      this.filePreviewLoading = true;
      this.$onboardingService
        .getFile(intervener.uuid, document.uuid)
        .then((res: any) => {
          this.filePreviewLoading = false;
          const urlCreator = window.URL || window.webkitURL;
          this.filePreview = urlCreator.createObjectURL(res);
          this.fileType = res.type;
          // this.fileName = this.tabs.title;
          // if (this.fileType === 'application/pdf') {
          //   this.filePreview = pdf.createLoadingTask(this.filePreview);
          //   this.filePreview.then((pdfRes: any) => {
          //     this.numPages = pdfRes.numPages;
          //   });
          // }
        })
        .catch((err: Error) => {
          console.error(err);
        });
    });
  }

  handleNext() {
    this.updateOnboardingState(this.lead_uuid, 15);
    //this.$router.push('paso16');
  }

  handleNextWithoutDocuments() {
    this.updateOnboardingState(this.lead_uuid, 15);
    //this.$router.push('paso16');
  }
}
