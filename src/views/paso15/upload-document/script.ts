/* eslint-disable  @typescript-eslint/no-explicit-any */
import { Component, Prop, Mixins } from 'vue-property-decorator';
import StepsMixin from '@/mixins/steps';

@Component
export default class UploadDocument extends Mixins(StepsMixin) {
  @Prop({ required: true }) intervener!: any;
  @Prop({ required: true }) document!: any;

  $refs!: any;

  filesArray: any[] = [];
  filenamesArray: any[] = [];
  categoriesArray = [];
  subcategoriesArray = [];
  maxSize = 300 * 1024 * 1024; // 300MB
  acceptedFormats = ['pdf', 'jpg', 'png'];
  loading = false;
  status: any = null;
  filePreview: any = '';

  created() {
    if (this.document.uuid) {
      this.getFile(this.document.uuid);
    }
  }

  onFileChange(e: any) {
    this.filesArray = [];
    this.filenamesArray = [];
    const selectedFiles = e.target.files;

    for (let i = 0; i < selectedFiles.length; i++) {
      if (selectedFiles[i].size < this.maxSize && this.correctType(selectedFiles[i])) {
        this.filesArray.push(selectedFiles[i]);
        this.filenamesArray.push(selectedFiles[i].name);
      } else if (selectedFiles[i].size > this.maxSize) {
        this.$bvToast.toast(`No se permite archivos más grandes de 300MB.`, {
          title: `Tamaño incorrecto`,
          autoHideDelay: 8000,
          variant: 'danger'
        });
      } else {
        this.$bvToast.toast(`Sólo se pueden subir documentos en formato ${this.formatsAcceptedInFileInput}`, {
          title: `Formato incorrecto`,
          autoHideDelay: 8000,
          variant: 'danger'
        });
      }
    }

    for (let i = 0; i < this.filesArray.length; i++) {
      console.warn(this.$refs);
      const reader = new FileReader();
      reader.onload = () => {
        if (this.correctType(this.filesArray[i])) {
          this.$refs.image[i].src = reader.result;
        }
      };
      reader.readAsDataURL(this.filesArray[i]);
    }
  }

  removeFile() {
    this.filesArray = [];
    this.filenamesArray = [];
  }

  async uploadFile() {
    this.loading = true;
    const payload = {
      uuid: this.intervener.uuid,
      files: this.filesArray,
      filesName: this.filenamesArray,
      category: this.document.key_category,
      subcategory: this.document.key_subcategory,
      validated: false
    };
    await this.$onboardingService
      .uploadFile(payload)
      .then((res: any) => {
        if (res.data && res.data.interveners_documents[0].uuid_document) {
          this.status = 'OK';
          this.filesArray = [];
          this.filenamesArray = [];
          const { uuid_document } = res.data.interveners_documents[0];
          this.getFile(uuid_document);
          this.$root.$emit('emit_fetch_data', true);
        } else {
          this.status = 'KO';
          this.loading = false;
        }
      })
      .catch((err: Error) => {
        this.loading = false;
        console.error(err);
      });
  }

  async getFile(uuid_document: string) {
    console.log('uuid_document', uuid_document);
    await this.$onboardingService
      .getFile(this.intervener.uuid, uuid_document)
      .then((res: any) => {
        console.log(res);
        const urlCreator = window.URL || window.webkitURL;
        this.filePreview = urlCreator.createObjectURL(res);
        // this.fileType = res.type;
        // this.fileName = this.tabs.title;
        // if (this.fileType === 'application/pdf') {
        //     this.filePreview = pdf.createLoadingTask(this.filePreview);
        //     this.filePreview.then(pdfRes => {
        //         this.numPages = pdfRes.numPages;
        //     });
        // }
      })
      .catch((err: Error) => {
        this.loading = false;
        console.error(err);
      });
  }

  correctType(file: any) {
    const extension = this.fileExtension(file.name);
    return this.acceptedFormats.find(x => extension.includes(x));
  }

  fileExtension(fname: string) {
    return fname.slice((Math.max(0, fname.lastIndexOf('.')) || Infinity) + 1);
  }

  get formatsAcceptedInFileInput() {
    return this.acceptedFormats.map(x => `.${x}`);
  }

  get hasDocuments() {
    return this.filesArray && this.filesArray.length >= 1 ? true : false;
  }
}
