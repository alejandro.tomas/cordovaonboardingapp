/* eslint-disable  @typescript-eslint/no-explicit-any */
import { Component, Prop, Mixins } from 'vue-property-decorator';
import StepsMixin from '@/mixins/steps';
import UploadDocument from '../upload-document/index.vue';

@Component({
  components: {
    UploadDocument
  }
})
export default class RowIntervener extends Mixins(StepsMixin) {
  @Prop({ required: true }) intervener!: any;
  @Prop({ required: false }) autoOpen!: boolean;
  documents = [];
  visibleUpload = '';

  mounted() {
    this.$onboardingService
      .getDocumentsValidation(this.intervener.uuid)
      .then((res: any) => {
        this.documents = res.data.interveners_documents;
        if (this.autoOpen) {
          const indexFirstDoc = this.documents.findIndex((x: any) => !x.uuid);
          if (indexFirstDoc >= 0) {
            this.showUploadSection(this.documents[indexFirstDoc]);
          }
        }
      })
      .catch((err: Error) => console.error(err));
  }

  showUploadSection(document: any) {
    setTimeout(() => {
      this.$root.$emit('emit_show_upload', this.intervener, document);
    }, 10);
  }

  showUploadMobileSection(document: any) {
    this.visibleUpload = `upload__${document.key_subcategory}`;
  }

  previewFile(document: any) {
    setTimeout(() => {
      this.$root.$emit('emit_preview_file', this.intervener, document);
    }, 10);
  }
}
