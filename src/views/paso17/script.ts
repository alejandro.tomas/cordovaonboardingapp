/* eslint-disable  @typescript-eslint/no-explicit-any */
import { Component, Mixins } from 'vue-property-decorator';
import StepsMixin from '@/mixins/steps';
import StepperStep from '@/interfaces/StepperStep';
import { AxiosResponse } from 'axios';

@Component
export default class Step17 extends Mixins(StepsMixin) {
  lead_uuid = '';
  steps: StepperStep[] = [];
  latestAccountState: any = {};

  created() {
    const { account } = this.$store.getters['applicationStore/applicationData'];
    this.$onboardingService.getAccountHistorical(account.uuid)
      .then((res: AxiosResponse) => {
        this.latestAccountState = res.data.accounts_historical.sort(
          (a: any, b: any) => new Date(b.created_on).getTime() - new Date(a.created_on).getTime()
        )[0];
        let activeStep = 1;
        if (this.latestAccountState.status === 'VALIDADO') {
          activeStep = 3;
        } else if (this.latestAccountState.status === 'ENVIADO') {
          activeStep = 2;
        }

        this.steps = [
          { name: 'Documentación recibida', active: activeStep === 1 },
          { name: 'Validación', active: activeStep === 2 },
          { name: 'Alta procesada', active: activeStep === 3 }
        ];
      })
      .catch((error: Error) => console.error(error));
  }

  handleNext() {
    this.$router.push('paso18');
  }
}
