/* eslint-disable  @typescript-eslint/no-explicit-any */
import { Component, Vue } from 'vue-property-decorator';
import router from '@/router';
import OnboardingState from '@/interfaces/OnboardingState';
import OnboardingStateBody from '@/interfaces/OnboardingStateBody';
import LegalPersonData from '@/interfaces/LegalPersonData';
import PhysicalPersonData from '@/interfaces/PhysicalPersonData';

@Component
export default class LegalPerson extends Vue {
  lead_uuid = '';
  isJuridicalEntity = false;
  prefix = '+34';
  email = '';
  mobile_phone = '';
  contact_type = '';
  name = '';
  document_number = '';
  document_date = '';
  is_document_permanent = false;
  birthdate = '';
  country = '';
  countries = [];
  options = [
    { text: 'Por email', value: 'email' },
    { text: 'Por teléfono', value: 'mobile_phone' },
    { text: 'A la dirección fiscal', value: 'fiscal' },
    { text: 'A la dirección postal', value: 'postal' }
  ];
  rolesOptions: any[] = [];

  created() {
    const data = this.$store.getters['applicationStore/applicationData'] as OnboardingStateBody;
    this.lead_uuid = data.lead_uuid;
    this.isJuridicalEntity = data.step4.isJuridicalEntity;
    this.email = data.step2.email;
    this.prefix = data.step2.mobile_phone_prefix;
    this.mobile_phone = data.step2.mobile_phone;

    this.loadRoles();
    this.$onboardingService.getCountries().then((res: any) => {
      const countriesList = res.countries_info;
      const i18nCountries: any = this.$t(`countries`);
      this.countries = countriesList.map((x: any) => {
        return { name: i18nCountries[x.country_key], key: x.country_key };
      });
    });
  }

  loadRoles() {
    const roles = this.$t(`intervenersStats.legal.all`);
    this.rolesOptions = Object.entries(roles).map(item => {
      return {
        key: item[0],
        name: item[1]
      };
    });
  }

  async handleNext(valid: boolean) {
    if (valid) {
      const stepData: LegalPersonData = {
        prefix: '+' + this.prefix,
        email: this.email,
        mobile_phone: this.mobile_phone,
        contact_type: this.contact_type,
        birthdate: this.birthdate,
        document_number: this.document_number,
        document_date: this.document_date,
        is_document_permanent: this.is_document_permanent,
        name: this.name,
        country: this.country
      };
      const intervener: PhysicalPersonData = {
        intervener_type: 'J',
        name: '',
        lastname_1: this.name,
        lastname_2: '',
        birthdate: this.birthdate,
        place_of_birth: this.country,
        country: this.country,
        document_type: 'CIF',
        document_number: this.document_number,
        document_date: this.document_date,
        is_document_permanent: this.is_document_permanent,
        nationality: this.country,
        addresses: {},
        roles: [this.rolesOptions.find(role => role.key === "TITULAR")]
      };
      const payload = {
        step: 'step6',
        data: stepData
      };
      if (this.$route.query && this.$route.query.pj) {
        const { interveners } = this.$store.getters['applicationStore/applicationData'];
        const intervenerStore = interveners[+this.$route.query.intervener - 1];
        intervener.id = +this.$route.query.intervener - 1;
        intervener.addresses = intervenerStore.addresses;
        intervener.roles = intervenerStore.roles;
        await this.$store.dispatch('applicationStore/setDataStep', payload);
        await this.$store.dispatch('applicationStore/setIntervenerById', intervener);
        this.$router.push({ path: 'paso14' });
      } else {
        await this.$store.dispatch('applicationStore/setDataStep', payload);
        await this.$store.dispatch('applicationStore/setIntervener', intervener);
        this.updateOnboardingState();
        router.push('/paso7');
      }
    }
  }

  updateOnboardingState() {
    const data = this.$store.getters['applicationStore/applicationData'];
    const newOnboardingState = { state_id: this.lead_uuid, actual_step: 6, state_json: data } as OnboardingState;
    this.$onboardingService.updateOnboardingState(newOnboardingState).catch((err: Error) => console.error(err));
  }

  handleDocumentPermanent(isPermanent: boolean) {
    if (isPermanent) {
      this.document_date = '';
    }
    this.is_document_permanent = isPermanent;
  }

  mounted() {
    if (this.$route.query && this.$route.query.pj) {
      const { interveners, step6 } = this.$store.getters['applicationStore/applicationData'];
      const intervener = interveners[+this.$route.query.intervener - 1];
      this.prefix = step6.prefix;
      this.email = step6.email;
      this.mobile_phone = step6.mobile_phone;
      this.contact_type = step6.contact_type;
      this.name = intervener.lastname_1;
      this.document_number = intervener.document_number;
      this.document_date = intervener.document_date;
      this.is_document_permanent = intervener.is_document_permanent;
      this.birthdate = intervener.birthdate;
      this.country = intervener.country;
    }
  }
}
