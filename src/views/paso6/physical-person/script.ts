/* eslint-disable  @typescript-eslint/no-explicit-any */
import { Component, Mixins, Watch } from 'vue-property-decorator';
import ValidationsMixin from '@/mixins/validations';
import OnboardingState from '@/interfaces/OnboardingState';
import PhysicalPersonData from '@/interfaces/PhysicalPersonData';
import OnboardingStateBody from '@/interfaces/OnboardingStateBody';

@Component
export default class PhysicalPerson extends Mixins(ValidationsMixin) {
  lead_uuid = '';

  // Campos del formulario
  birthdate = '';
  country: any = '';
  document_date = '';
  is_document_permanent = false;
  document_number = '';
  document_type: any = {};
  gender = '';
  lastname_1 = '';
  lastname_2 = '';
  marital_status: any = '';
  name = '';
  nationality: any = '';
  place_of_birth: any = '';

  // Campos de opciones/configuración
  dateMaxAge: any = '';
  dateMinAge: any = '';
  genderOptions = [
    { text: 'Hombre', value: 'V' },
    { text: 'Mujer', value: 'H' }
  ];
  countries: any = [];
  documentTypeOptions = [
    { name: 'NIF', key: 'NIF' },
    { name: 'Pasaporte', key: 'PASAPORTE' }
  ];
  maritalStatusesOptions: any[] = [];
  isAddingSecondaryIntervener = false;

  videoid: any = null;
  comunicationType = 'EMAIL';
  comunicationTypeOptions = [
    { text: 'Correo', value: 'POSTAL' },
    { text: 'Email', value: 'EMAIL' },
    { text: 'SMS', value: 'SMS' },
    { text: 'Notificaciones Push', value: 'PUSH' }
  ];

  documentFrontImage = null;
  documentBackImage = null;

  @Watch('$route', { immediate: true, deep: true })
  params(p: any) {
    if (p.query && p.query.id) {
      const id = p.query.id;
      this.$onboardingService
        .getDataFinverisId(id)
        .then((res: any) => {
          const dataFinveris = res.result.data;
          //this.name = dataFinveris.name || '';
          //this.lastname_1 = dataFinveris.surname || '';
          this.document_type = dataFinveris.passportCard
            ? { name: 'Pasaporte', key: 'PASAPORTE' }
            : { name: 'NIF', key: 'NIF' };
          this.document_number = dataFinveris.card;
          this.gender = dataFinveris.gender;
          this.videoid = id;
        })
        .catch((err: Error) => {
          console.error(err);
          this.$bvToast.toast(`${err.message}`, {
            title: `Error`,
            autoHideDelay: 5000,
            variant: 'danger'
          });
        });
    }
  }

  created() {
    this.setMinMaxBirthdate();
    const data: OnboardingStateBody = this.$store.getters['applicationStore/applicationData'];

    this.isAddingSecondaryIntervener = data.isAddingSecondaryIntervener || data.isJuridicalEntity;
    this.lead_uuid = data.lead_uuid;

    if (!data.isJuridicalEntity && data.settedIsJuridicalEntity && data.interveners.length === 0) {
      this.name = data.step2.name;
      this.lastname_1 = data.step2.lastname_1;
      this.lastname_2 = data.step2.lastname_2;
    }
    this.loadMaritalStatusOptions();
  }

  mounted() {
    this.$onboardingService.getCountries().then((res: any) => {
      const countriesList = res.countries_info;
      const i18nCountries: any = this.$t(`countries`);
      this.countries = countriesList.map((x: any) => {
        return { name: i18nCountries[x.country_key], key: x.country_key };
      });
      this.countries.sort(this.compare);
      this.loadIntervenerToEdit();
    });
  }

  compare(a: any, b: any) {
    // Use toUpperCase() to ignore character casing
    const countryA = a.name.toUpperCase();
    const countryB = b.name.toUpperCase();

    let comparison = 0;
    if (countryA > countryB) {
      comparison = 1;
    } else if (countryA < countryB) {
      comparison = -1;
    }
    return comparison;
  }

  loadMaritalStatusOptions() {
    const maritalStatuses = this.$t(`maritalStatuses`);
    this.maritalStatusesOptions = Object.entries(maritalStatuses).map(item => {
      return {
        key: item[0],
        name: item[1]
      };
    });
  }

  /**
   * Configuramos la edad mínima y máxima con la que un usuario
   * puede darse de alta en el OnBoarding
   */
  setMinMaxBirthdate() {
    const maxYears = 100; // 90 años como edad máxima
    const minYears = 0; // 0 años como edad mínima

    const now = new Date();
    const today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
    const maxAge = new Date(today);
    maxAge.setFullYear(maxAge.getFullYear() - maxYears);
    this.dateMaxAge = maxAge;

    const minAge = new Date(today);
    minAge.setFullYear(minAge.getFullYear() - minYears);
    this.dateMinAge = minAge;
  }

  handleNext(valid: boolean) {
    if (valid) {
      const dataForm: PhysicalPersonData = {
        intervener_type: 'F',
        name: this.name,
        lastname_1: this.lastname_1,
        lastname_2: this.lastname_2,
        birthdate: this.birthdate,
        place_of_birth: this.place_of_birth,
        country: this.country,
        gender: this.gender,
        document_type: this.document_type,
        document_number: this.document_number,
        document_date: this.document_date,
        is_document_permanent: this.is_document_permanent,
        nationality: this.nationality,
        marital_status: this.marital_status,
        addresses: {},
        percent: 0,
        videoid: this.videoid !== null ? this.videoid : null,
        comunicationType: this.comunicationType,
        documentFrontImage: this.documentFrontImage != null ? this.documentFrontImage : null,
        documentBackImage: this.documentBackImage != null ? this.documentBackImage : null
      };

      if (this.$route.query && this.$route.query.intervener) {
        const { interveners } = this.$store.getters['applicationStore/applicationData'];
        const intervener = interveners[+this.$route.query.intervener - 1];
        dataForm.id = +this.$route.query.intervener - 1;
        dataForm.addresses = intervener.addresses;
        dataForm.roles = intervener.roles;
        this.$store.dispatch('applicationStore/setIntervenerById', dataForm);
        this.$router.push({ path: 'paso7', query: { intervener: this.$route.query.intervener } });
      } else {
        dataForm.secondary = this.isAddingSecondaryIntervener;
        this.$store.dispatch('applicationStore/setIntervener', dataForm);
        this.updateOnboardingState();
        this.$router.push('/paso7');
      }
    }
  }

  updateOnboardingState() {
    const data = this.$store.getters['applicationStore/applicationData'];
    const newOnboardingState = { state_id: this.lead_uuid, actual_step: 6, state_json: data } as OnboardingState;
    this.$onboardingService.updateOnboardingState(newOnboardingState).catch((err: Error) => console.error(err));
  }

  handleDocumentPermanent(isPermanent: boolean) {
    if (isPermanent) {
      this.document_date = '';
    }
    this.is_document_permanent = isPermanent;
  }

  loadIntervenerToEdit() {
    // Si tenemos por query el parámetro 'intervener' significa que desde la pantalla de
    // resumen el usuario quiere editar un interviniente.
    // Nota: Se le resta -1 para acceder al array de intervinientes en el caso de que sea
    // en la posición 0.
    if (this.$route.query && this.$route.query.intervener) {
      const { interveners } = this.$store.getters['applicationStore/applicationData'];
      const intervener = interveners[+this.$route.query.intervener - 1];

      this.birthdate = intervener.birthdate;
      this.country = this.countries.find((country: any) => country.key === intervener.country.key);
      this.document_date = intervener.document_date;
      this.is_document_permanent = intervener.is_document_permanent;
      this.document_number = intervener.document_number;
      this.document_type = this.documentTypeOptions.find((documentType: any) => documentType.key === intervener.document_type.key);
      this.gender = intervener.gender;
      this.lastname_1 = intervener.lastname_1;
      this.lastname_2 = intervener.lastname_2;
      this.marital_status = this.maritalStatusesOptions.find((maritalStatus: any) => maritalStatus.key === intervener.marital_status.key);
      this.name = intervener.name;
      this.nationality = this.countries.find((country: any) => country.key === intervener.nationality.key);
      this.place_of_birth = this.countries.find((country: any) => country.key === intervener.place_of_birth.key);
    }
  }
}
