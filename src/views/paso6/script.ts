import { Component, Vue } from 'vue-property-decorator';
// import YuriHeader from '../../components/yuri-header/index.vue'
import PhysicalPerson from './physical-person/index.vue';
import LegalPerson from './legal-person/index.vue';
@Component({
  components: {
    PhysicalPerson,
    LegalPerson
  }
})
export default class Step5 extends Vue {
  isJuridicalEntity = false;
  lead_uuid = '';
  isValid = false;

  created() {
    const { lead_uuid, isJuridicalEntity, settedIsJuridicalEntity, createInterveners } = this.$store.getters[
      'applicationStore/applicationData'
    ];
    this.lead_uuid = lead_uuid;
    this.isJuridicalEntity = isJuridicalEntity && settedIsJuridicalEntity && !createInterveners;
    if (this.$route.query && this.$route.query.pj) {
      this.isJuridicalEntity = true;
    }
    this.checkIsDigitallyIdentified();
  }

  checkIsDigitallyIdentified() {
    if (this.$route.query.digitally_identified) {
      this.$onboardingService.updateLead({ uuid: this.lead_uuid, digitally_identified: true });
    }
  }
}
