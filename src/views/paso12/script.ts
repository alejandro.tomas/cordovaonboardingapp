/* eslint-disable  @typescript-eslint/no-non-null-assertion */
import { Component, Mixins } from 'vue-property-decorator';
import OnboardingState from '@/interfaces/OnboardingState';
import Step12State from '@/interfaces/Step12State';
import StepsMixin from '@/mixins/steps';
import { AxiosResponse } from 'axios';

@Component
export default class Step12 extends Mixins(StepsMixin) {
  lead_uuid = '';
  loading = false;
  questions: any;
  answers: any;

  created() {
    const { lead_uuid } = this.$store.getters['applicationStore/applicationData'];
    this.lead_uuid = lead_uuid;
    this.getConvenienceTest();
  }

  getConvenienceTest() {
    this.loading = true;
    this.$onboardingService
      .getConvenienceTest()
      .then((res: any) => {
        this.questions = res.convenience_test_questions;
        this.loading = false;
        this.loadForm();
      })
      .catch((err: Error) => console.error(err));
  }

  getQuestions(section: number) {
    return this.questions.filter((x: any) => x.section === section);
  }

  handleNext() {
    // Siento hacer esto así :S
    if (this.answers[0].answer.length === 1) {
      this.answers[0].answer = this.answers[0].answer[0];
    }
    if (this.answers[1].answer.length === 1) {
      this.answers[1].answer = this.answers[1].answer[0];
    }
    this.updateOnboardingState(this.lead_uuid, 12);
    this.$onboardingService.setConvenienceTest(this.lead_uuid, { responses: this.answers })
      .then((res: AxiosResponse<any>) => {
        const payload: { step: string; data: Step12State } = {
          step: 'step12',
          data: {
            responses: this.answers,
            score: res.data.total_score
          }
        };
        this.$store.dispatch('applicationStore/setDataStep', payload);
        this.updateLead('/paso13');
      })
      .catch((err: Error) => console.error(err));
  }

  updateLead(nextStep: string) {
    // Actualizamos el lead
    const data = this.$store.getters['applicationStore/applicationData'];
    const newOnboardingState = {
      state_id: data.lead_uuid,
      actual_step: 12,
      state_json: data
    } as OnboardingState;
    this.$onboardingService
      .updateOnboardingState(newOnboardingState)
      .then(() => {
        // Redirección a la siguiente pantalla
        this.$router.push(nextStep);
      })
      .catch((err: Error) => console.error(err));
  }

  loadForm() {
    this.answers = this.questions.map((question: any) => {
      return {
        uuid_question: question.uuid,
        question_key: question.question_key,
        answer: []
      };
    });
  }

  setAnswer(question: any, answer: any, value: any, multiple: boolean) {
    const indexQuestion = this.answers.findIndex((q: any) => q.uuid_question === question.uuid);
    if (multiple) {
      const existResponse = this.answers[indexQuestion].answer.findIndex((q: any) => q === answer);
      if (existResponse < 0) {
        this.answers[indexQuestion].answer = [...this.answers[indexQuestion].answer, answer];
      } else {
        this.answers[indexQuestion].answer.splice(existResponse, 1);
      }
    } else {
      this.answers[indexQuestion].answer = answer;
    }
    console.warn(this.answers);
  }
}
