export default interface KycQuestion {
    answer: any[];
    id_poll: number;
    id_question: number;
    uuid_intervener: string;
}