export default interface LeadIntervenerRole {
    uuid_lead: string;
    uuid_intervener: string;
    role: string;
}