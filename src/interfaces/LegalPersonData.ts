export default interface LegalPersonData {
    prefix: string;
    email: string;
    mobile_phone: string;
    contact_type: string;
    name: string;
    document_number: string;
    document_date: string;
    is_document_permanent: boolean;
    birthdate: string;
    country: any;
}