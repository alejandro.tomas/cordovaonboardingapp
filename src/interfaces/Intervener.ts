import Address from './Address';

export default interface Intervener {
    personal_data: PersonalData;
    contacts: ContactData;
    fiscal_address: FiscalAddress;
    postal_address: Address[];
    business: any;
    crs: any;
    fatca: any;
    account_relationship: AccountRelationship;
}

interface PersonalData {
    name: string;
    lastname_1: string;
    lastname_2: string;
    intervener_type: string;
    birthdate: string;
    document_expiration_date: string;
    document_type: string;
    document_id: string;
    is_document_permanent: boolean;
    key_nationality_1: string;
    key_country_of_birth: string;
    gender: string;
    marital_status: string;
}

interface ContactData {
    email: string;
    key_mobile_phone_prefix: string;
    mobile_phone: string;
}

interface FiscalAddress {
    f_address_street_type: string;
    f_address_street_1: string;
    f_address_number: string;
    f_address_floor: string;
    f_address_door: string;
    f_address_postal_code: string;
    f_address_city: string;
    f_address_state: string;
    f_key_address_country: string;
}

interface AccountRelationship {
    account_intervener_role: string;
    holding_percentage: string;
}