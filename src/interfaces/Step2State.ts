export default interface Step2State {
    name: string;
    lastname_1: string;
    lastname_2: string;
    email: string;
    mobile_phone_prefix: string;
    mobile_phone: string;
    f_address_postal_code: string;
}