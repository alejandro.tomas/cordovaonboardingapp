export default interface Address {
    street_type: any;
    street_1: string;
    number: string;
    floor: string;
    door: string;
    postal_code: string;
    city: string;
    state: any;
    key_country: any;
}