export default interface Step3State {
    responses: Response[];
    failed: boolean;
}