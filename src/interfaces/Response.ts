export default interface Response {
    uuid_lead_client: string;
    uuid_question: string;
    selected_response: number;
}