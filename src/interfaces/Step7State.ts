import Address from './Address';

export default interface Step7State {
    fiscal_address: Address;
    postal_address: Address;
}