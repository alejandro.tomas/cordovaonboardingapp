export default interface Step12State {
    responses: any[];
    score: number;
}