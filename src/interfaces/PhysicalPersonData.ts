import Address from './Address';

export default interface PhysicalPersonData {
    intervener_type: string;
    id?: number;
    name: string;
    lastname_1: string;
    lastname_2: string;
    birthdate: string;
    place_of_birth: any;
    country: any;
    gender?: string;
    document_type: any;
    document_number: string;
    document_date: string;
    is_document_permanent: boolean;
    nationality: any;
    marital_status?: any;
    addresses: Addresses;
    roles?: any[];
    is_signature_required?: boolean;
    email?: string;
    percent?: number;
    kyc?: boolean;
    mobile_phone?: string;
    key_mobile_phone_prefix?: string;
    secondary?: boolean;
    originalIndex?: number;
    videoid?: any;
    comunicationType?: string;
    documentFrontImage?: any;
    documentBackImage?: any;
}

interface Addresses {
    fiscal_address?: Address;
    postal_address?: Address;
}