export default interface Step13State {
    id?: number;
    productType: string;
    selectedOperation: any;
    selectedProduct: any;
    operationData: any;
}