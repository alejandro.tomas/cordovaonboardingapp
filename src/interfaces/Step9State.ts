export default interface Step9State {
    gdpr_1: boolean;
    gdpr_2: boolean;
    gdpr_3: boolean;
    gdpr_4: boolean;
}