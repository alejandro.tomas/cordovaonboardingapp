export default interface Lead {
    uuid: string;
    f_address_postal_code: string;
    name: string;
    lastname_1: string;
    lastname_2: string;
    mobile_phone: string;
    mobile_phone_prefix: string;
    email: string;
    lopd: boolean;
    sent_reminder_email: boolean;
    is_juridical_entity: boolean;
}