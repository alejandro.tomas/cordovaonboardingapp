export default interface StepperStep {
    name: string;
    active: boolean;
}