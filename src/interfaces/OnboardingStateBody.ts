import Step2State from './Step2State';
import Step3State from './Step3State';
import Step4State from './Step4State';
import Step9State from './Step9State';
import Step10State from './Step10State';
import Step7State from './Step7State';
import PhysicalPersonData from './PhysicalPersonData';
import Step13State from './Step13State';
import Step11State from './Step11State';
import KycQuestion from './KycQuestion';
import Step12State from './Step12State';
import Operation from './Operation';
import Step14State from './Step14State';

export default interface OnboardingStateBody {
    lead_uuid: string;
    operationsType: string;
    isEditing: boolean;
    isAddingSecondaryIntervener: boolean;
    agent: any;
    isJuridicalEntity: boolean;
    settedIsJuridicalEntity: boolean;
    createInterveners: boolean;
    interveners: PhysicalPersonData[];
    operations: Operation[];
    step2: Step2State;
    step3: Step3State;
    step4: Step4State;
    step5: any;
    step6: any;
    step7: Step7State;
    step8: KycQuestion[];
    step9: Step9State;
    step10: Step10State;
    step11: Step11State;
    step12: Step12State;
    step13: Step13State;
    step14: Step14State;
}