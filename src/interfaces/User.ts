export default interface User {
    username: string;
    password: string;
    given_name: string;
    family_name: string;
}