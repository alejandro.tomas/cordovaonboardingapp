export default interface Operation {
    operationData: any;
    productType: string;
    selectedOperation: any;
    selectedProduct: any;
}