import OnboardingStateBody from './OnboardingStateBody';

export default interface OnboardingState {
    state_id: string;
    state_json: OnboardingStateBody;
    actual_step: number;
}