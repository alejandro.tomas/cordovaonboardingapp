/* eslint-disable no-param-reassign */

// Install BootstrapVue
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";

// Axios
import axios from "axios";

// VeeValidate
import { ValidationProvider, ValidationObserver } from "vee-validate";
import { extend } from "vee-validate";
import { required, email, numeric } from "vee-validate/dist/rules";

// Custom services
import OnBoardingService from "../services/onboarding.service";

// Custom components
import components from "../../components/components";
import validateCIF from "../../utils/validateCif";
import validateDNI from "../../utils/validateDni";

const APP_LOG_PREFIX = "[OnBoarding]";

import VueCordova from "vue-cordova";

const OnBoardingCore = {
  install(Vue: any) {
    console.info("- Installing OnBoarding plugin...");

    // Bootstrap Vue
    Vue.use(BootstrapVue);
    Vue.use(IconsPlugin); // Opcional
    Vue.use(VueCordova);

    // VeeValidate
    Vue.component("ValidationProvider", ValidationProvider);
    Vue.component("ValidationObserver", ValidationObserver);

    // No message specified.
    extend("email", {
      ...email,
      message: '"{_field_}" tiene un formato incorrecto.',
    });
    extend("numeric", numeric);

    // Override the default message.
    extend("required", {
      ...required,
      message: "Campo requerido.",
    });

    extend("min", {
      validate(value, args) {
        return value.length >= args.length;
      },
      params: ["length"],
      message: '"{_field_}" debe tener al menos {length} carácteres.',
    });

    extend("min_value", {
      validate(value, args: any) {
        return +value >= +args.min;
      },
      params: ["min"],
      message: '"{_field_}" debe tener un valor igual o superior a {min}',
    });

    extend("max", {
      validate(value, args) {
        return value.length <= args.length;
      },
      params: ["length"],
      message: '"{_field_}" debe tener máximo {length} carácteres.',
    });

    extend("cif", {
      validate(value) {
        return validateCIF(value);
      },
      message: '"{_field_}" tiene un formato incorrecto.',
    });

    extend("dni", {
      validate(value) {
        return validateDNI(value);
      },
      message: '"{_field_}" tiene un formato incorrecto.',
    });

    extend("password", {
      params: ["target"],
      validate(value, { target }: any) {
        return value === target;
      },
      message: "Las contraseñas no coinciden",
    });

    /* Global register all components */
    Object.keys(components).forEach((k) => {
      console.log(
        `🛠%c Componente registrado%c ${k}`,
        "color: green",
        "font-weight: bold"
      );
      Vue.component(k, components[k]);
    });

    // Axios
    Vue.prototype.$axios = axios;
    Vue.prototype.$onboardingService = OnBoardingService;
    Vue.prototype.$appName = "Mi aplicación";

    Vue.prototype.$log = {
      info: (text: string) => console.info(`${APP_LOG_PREFIX} - ${text}`),
      error: (text: string) => console.error(`${APP_LOG_PREFIX} - ${text}`),
      debug: (text: string) => console.log(`${APP_LOG_PREFIX} - ${text}`),
      warn: (text: string) => console.warn(`${APP_LOG_PREFIX} - ${text}`),
    };
  },
};

export default OnBoardingCore;
