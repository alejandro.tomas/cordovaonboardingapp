/* eslint-disable  @typescript-eslint/no-explicit-any */
import OnBoardingService from "./onboarding.service";

const ServicesPlugin = {
    /**
     * Installs plugin on vue project.
     * @param {Object} Vue Vue Object
     * @param {Object} Config Configuration of the plugin
     */
    install(Vue: any, { router }: { router: any }) {

        console.info('Installing OnBoarding Library...');
        if (!router) {
            throw new Error('Router object is required.');
        }
        Vue.prototype.$onboardingService = OnBoardingService;
    }
};

declare module 'vue/types/vue' {
    interface Vue {
        $onboardingService: any;
    }
}

export default ServicesPlugin;