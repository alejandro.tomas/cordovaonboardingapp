import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
axios.defaults.adapter = require('axios/lib/adapters/http');

export class FinverisService {
    private connection: AxiosInstance;
    private CONFIG: AxiosRequestConfig = {
        baseURL: process.env.VUE_APP_FINVERIS_BASE_URL,
        withCredentials: false,
        headers: {
            Accept: '*/*'
        }
    };

    private finverisAuthBody: any = {
        "client_id": process.env.VUE_APP_FINVERIS_CLIENT_ID,
        "client_secret": process.env.VUE_APP_FINVERIS_CLIENT_SECRET,
        "grant_type": process.env.VUE_APP_FINVERIS_GRANT_TYPE,
        "username": process.env.VUE_APP_FINVERIS_USERNAME,
        "password": process.env.VUE_APP_FINVERIS_PASSWORD
    };

    private static NO_SIGNERS_ERROR_MESSAGE = 'No se han especificado los firmantes del paquete';
    private static NO_DOCUMENTS_ERROR_MESSAGE = 'No se han especificado documentos que crear o enlazar';
    private static DOCUMENTS_TO_SIGN_AND_IDS_ERROR_MESSAGE = 'No se pueden crear y enlazar documentos a la vez';

    private static AUTHENTICATE_URL = '/oauth/token';
    private static CREATE_PACKAGE_URL = '/package/send';

    public constructor() {
        this.connection = axios.create(this.CONFIG);
    }

    private authenticateOnFinveris(): Promise<AxiosResponse<any>> {
        const params = new URLSearchParams();
        params.append('client_id', this.finverisAuthBody['client_id']);
        params.append('client_secret', this.finverisAuthBody['client_secret']);
        params.append('grant_type', this.finverisAuthBody['grant_type']);
        params.append('username', this.finverisAuthBody['username']);
        params.append('password', this.finverisAuthBody['password']);

        return this.connection.post(
            `${this.CONFIG.baseURL}${FinverisService.AUTHENTICATE_URL}`, 
            params, 
            {
                baseURL: this.CONFIG.baseURL,
                withCredentials: false,
                headers: {
                    "Accept": "*/*",
                    "Connection": "keep-alive",
                    "Content-Type": 'application/x-www-form-urlencoded'
                }
            }
        );
    }

    private checkIfValidDataAndReturnData(
        packageName: string,
        finverisPackageType: FinverisPackageType,
        signersIds: string[],
        finverisSecurityType: FinverisSecurityType,
        documentsToSign?: DocumentToSign[],
        documentsToSignId?: DocumentToSignId[]
        ): void {
        const noDocumentsToSign = (documentsToSign == null || documentsToSign.length == 0);
        const noDocumentsToSignIDs = (documentsToSignId == null || documentsToSignId.length == 0);
        const noSignersIds = signersIds == null || signersIds.length == 0;

        if (noSignersIds) {
            throw new Error(FinverisService.NO_SIGNERS_ERROR_MESSAGE);
        }

        if (noDocumentsToSign && noDocumentsToSignIDs) {
            throw new Error(FinverisService.NO_DOCUMENTS_ERROR_MESSAGE);
        }

        if (!noDocumentsToSign && !noDocumentsToSignIDs) {
            throw new Error(FinverisService.DOCUMENTS_TO_SIGN_AND_IDS_ERROR_MESSAGE);
        }

        const body: any = {
            "name": packageName,
            "packageType": finverisPackageType,
            "signersIds": signersIds,
            "useExternalUsername": true,
            "security": finverisSecurityType
        }

        if (!noDocumentsToSign) {
            body['documentsToSign'] = documentsToSign;
        }
        else {
            body['documentsToSignId'] = documentsToSignId;
        }

        return body;
    }

    public sendDocumentsPackageToSign(
        packageName: string,
        finverisPackageType: FinverisPackageType,
        signersIds: string[],
        finverisSecurityType: FinverisSecurityType,
        documentsToSign?: DocumentToSign[],
        documentsToSignId?: DocumentToSignId[]
    ): Promise<any>
    {
        const body = this.checkIfValidDataAndReturnData(packageName, finverisPackageType, signersIds, finverisSecurityType, documentsToSign, documentsToSignId);

        return this.authenticateOnFinveris()
        .then((authResponse: AxiosResponse) => {
            if (authResponse != null && authResponse.status == 200 && authResponse.data != null) {
                const authResponseBody = authResponse.data;

                if (authResponseBody != null && authResponseBody.access_token != null) {
                    const token = authResponseBody.access_token;
                    
                    return this.connection.post(
                        `${this.CONFIG.baseURL}${FinverisService.CREATE_PACKAGE_URL}`, 
                        body,
                        {
                            headers: {
                                "Accept": '*/*',
                                "Authorization": `Bearer ${token}`,
                                "Content-Type": 'application/json'
                            }
                        }
                    );
                }
                console.log(`Failed the authentication call. Status: ${authResponse.status}, Body: ${authResponseBody.data}`);
                Promise.reject(`Failed the authentication call. Status: ${authResponse.status}, Body: ${authResponseBody.data}`);
            }
            
            console.log(`Invalid response on Finveris authentication call`);
            Promise.reject(`Invalid response on Finveris authentication call`);
        })
        .catch((err) => {
            console.log(JSON.stringify(err));
            Promise.reject(`Failed the authentication call. ${err}`);
        });
    }
}

/*
    Por defecto emplearemos TYPE_UNSPECIFIED
*/
export enum FinverisPackageType {
    TYPE_ANALYSIS = 'TYPE_ANALYSIS',
    TYPE_CONDITION = 'TYPE_CONDITION',
    TYPE_CONTRACT = 'TYPE_CONTRACT',
    TYPE_MARKET = 'TYPE_MARKET',
    TYPE_PORTFOLIO = 'TYPE_PORTFOLIO',
    TYPE_OTHER = 'TYPE_OTHER',
    TYPE_PURCHASE = 'TYPE_PURCHASE',
    TYPE_RECOMMENDATION = 'TYPE_RECOMMENDATION',
    TYPE_SALE = 'TYPE_SALE',
    TYPE_STATEMENT = 'TYPE_STATEMENT',
    TYPE_SUITABILITY_TEST = 'TYPE_SUITABILITY_TEST',
    TYPE_TRANSFER = 'TYPE_TRANSFER',
    TYPE_UNSPECIFIED = 'TYPE_UNSPECIFIED'
}

/*
    Por defecto emplearemos ONLY_SMS
*/
export enum FinverisSecurityType {
    ONLY_SMS = 'ONLY_SMS',
    SMS_AND_PASSWORD = 'SMS_AND_PASSWORD'
}

/*
    Por defecto sendToThird = false y forceToOpen = false

    content es el documento como un string en base 64
*/
export interface DocumentToSign {
    fileName: string;
    sendToThird?: boolean;
    content: string;
    forceToOpen?: boolean;
}

/*
    Por defecto sendToThird = false y forceToOpen = false

    fileId es un identificador interno de Finveris.
    
    No se usará esta interfaz/modo de creación de paquete salvo que se requiera
    en algún caso concreto.
*/
export interface DocumentToSignId {
    fileId: string;
    sendToThird?: boolean;
    forceToOpen?: boolean;
}