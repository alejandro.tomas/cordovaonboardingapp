/* eslint-disable  @typescript-eslint/no-explicit-any */
import authService from './auth.service';
import axios, { AxiosResponse, AxiosInstance } from 'axios';
import { v4 as uuidv4 } from 'uuid';
import Axios from 'axios';

const responseHandler = (response: AxiosResponse) => {
    if (!response.data) {
        throw new Error('Response has no expected structure');
    }
    return response.data;
};

// TODO: Mejorar manejo de error -> En un 401(no autorizado) ¿no deberíamos hacer un logout?
const errorResponseHandler = async (errorResponse: any) => {
    // By default: error setting up request
    console.warn('errorResponse');
    console.warn(errorResponse);
    console.warn(errorResponse.response);
    if (errorResponse.response.status === 401) {
        if (errorResponse.config._retry) {
            console.warn('Error 401');
            const err = new Error();
            err.name = 'Error 401';
            err.message = 'No autorizado';
            throw err;
        } else {
            const response = await authService.getAccessToken();
            await authService.saveAuthData(response.data);
            errorResponse.config._retry = true;
            errorResponse.config.headers.Authorization = "Bearer " + response.data.access_token;
            return Axios.request(errorResponse.config);
        }
    }

    if (errorResponse.response.status === 404) {
        console.warn('Error 404');
        const err = new Error();
        err.name = 'Error 404';
        err.message = 'No encontrado';
        throw err;
    }

    const err = new Error(errorResponse.response);
    err.name = errorResponse.response.status ? `${errorResponse.response.status}` : `HTTP Error`;
    if (errorResponse.response.data && errorResponse.response.data.result) {
        err.message = errorResponse.response.data.result.info;
        if (errorResponse.response.data.result.errors && errorResponse.response.data.result.errors[0]) {
            err.message = `${errorResponse.response.data.result.errors[0].message}`;
        }
    } else {
        err.message = 'No response was received';
    }
    throw err;
};

/**
 * @override
 */
class ApiService {
    connection: AxiosInstance;
    CONFIG = {
        baseURL: process.env.VUE_APP_USERS_API,
        withCredentials: false,
        headers: {
            Accept: '*/*',
            'Content-Type': 'application/json',
            // 'x-access-token': 'eyJ1c2VybmFtZSI6ICJqdWFucGVkcm8ucGluYXJAY2xvdWRhcHBpLm5ldCJ9',
            'X-Request-Id': uuidv4()
        }
    };

    constructor() {
        this.connection = axios.create(this.CONFIG);
        this.connection.interceptors.request.use(
            authService.authorizationInterceptor,
            authService.authorizationInterceptorOnError
        );
        this.connection.interceptors.response.use(responseHandler, errorResponseHandler);
    }

    /**
     * @override
     */
    get(url: string, config = {}) {
        return this.connection.get(url, config);
    }

    /**
     * @override
     */
    post(url: string, payload: any, config = {}) {
        return this.connection.post(url, payload, config);
    }

    /**
     * @override
     */
    put(url: string, payload: any, config = {}) {
        return this.connection.put(url, payload, config);
    }

    /**
     * @override
     */
    patch(url: string, payload: any, config = {}) {
        return this.connection.patch(url, payload, config);
    }

    /**
     * @override
     */
    delete(url: string, config = {}) {
        return this.connection.delete(url, config);
    }
}

export default ApiService;
