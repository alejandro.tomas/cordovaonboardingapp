/* eslint-disable  @typescript-eslint/no-explicit-any */
import ApiService from './api.service';
import Response from '@/interfaces/Response';
import Lead from '@/interfaces/Lead';
import OnboardingState from '@/interfaces/OnboardingState';
import User from '@/interfaces/User';
import Intervener from '@/interfaces/Intervener';
import LeadIntervenerRole from '@/interfaces/LeadIntervenerRole';
import { AxiosResponse } from 'axios';

class OnBoardingService extends ApiService {
  getUrl = (url: string) => {
    return this.get(url);
  };

  createLead = (payload: any) => {
    return this.post('users-api/leads', payload);
  };

  getLead = (lead_uuid: string) => {
    return this.get(`users-api/onboarding/state/${lead_uuid}`);
  };

  getRoles = () => {
    return this.get(`interveners-api/roles`);
  };

  getTestQuestions = () => {
    return this.get('users-api/leads-test');
  };

  getCountries = () => {
    return this.get('users-api/onboarding/countries-info');
  };

  getConvenienceTest = () => {
    return this.get('users-api/onboarding/convenience-test/questions');
  };

  setConvenienceTest = (lead_uuid: string, payload: any) => {
    return this.post(`users-api/onboarding/convenience-test/answers/${lead_uuid}`, payload);
  };

  getProvinces = (code: string) => {
    return this.get(`users-api/onboarding/provinces/${code}`);
  };

  getAgent = (postalCode: string | number) => {
    return this.get(`users-api/agents/${postalCode}`);
  };

  sendEmail = (email: string, lead_uuid: string) => {
    return this.post('users-api/onboarding/email', { email: email, identification_code: lead_uuid });
  };

  submitTest = (test: Response[]) => {
    return this.post('users-api/leads-test', { responses: test });
  };

  createLeadPersonTypeData = (data: Lead, type: string, lead_uuid: string) => {
    return this.post(`users-api/leads/${type}/${lead_uuid}`, data);
  };

  updateLead = (data: Lead) => {
    return this.patch('users-api/leads/' + data.uuid, data);
  };

  updateOnboardingState = (data: OnboardingState) => {
    return this.put('users-api/onboarding/state/' + data.state_id, data);
  };

  getVideoIdentificationUrl = (lead_uuid: string) => {
    return this.post('users-api/onboarding/video-identification', {
      success: `${location.protocol}//${location.host}/onboarding/paso6`,
      error: `${location.protocol}//${location.host}/onboarding/digital-error?uuid=${lead_uuid}`
    });
  };
  getDataFinverisId = (uuid: string) => {
    return this.get(`users-api/onboarding/finveris/${uuid}`);
  };
  createUser = (data: User) => {
    return this.post('users-api/onboarding/user', data);
  };

  createIntervener = (data: Intervener) => {
    return this.post('interveners-api/interveners', { interveners: [data] });
  };

  getKycQuestions = () => {
    return this.get('accounts-api/polls');
  };

  sendKycResponses = (data: any) => {
    return this.post('accounts-api/polls/answers', data);
  };

  createLeadIntervenerRoleRelations = (relations: LeadIntervenerRole[]) => {
    return this.post('users-api/onboarding/lead-intervener-role', { relations: relations });
  };

  getProducts = () => {
    return this.get(`/accounts-api/products`);
  };

  login = (credentials: any): Promise<AxiosResponse> => {
    return this.post(`/users-api/onboarding/login`, credentials);
  };

  createAccount = (payload: any) => {
    return this.post(`/accounts-api/accounts`, payload);
  };

  createOperations = (payload: any) => {
    return this.post(`/accounts-api/operations`, payload);
  };

  getIntervenersAccount = (uuid: string) => {
    return this.get(`/accounts-api/accounts/${uuid}/interveners?$limit=25&$offset=0`);
  };

  getAccountDocumentsValidation = (uuid: string) => {
    return this.get(`/accounts-api/accounts/${uuid}/document-validations?$limit=25&$offset=0`);
  };

  getDocumentsValidation = (uuidIntervener: string) => {
    return this.get(`/interveners-api/interveners/${uuidIntervener}/document-validations?$limit=25&$offset=0`);
  };

  getDocumentsOperations = (uuidOperation: string) => {
    return this.get(`/accounts-api/operations/${uuidOperation}/document-validations?$limit=25&$offset=0`);
  };

  uploadFile = (payload: any) => {
    const config = {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    };

    const formData = new FormData();
    for (let i = 0; i < payload.files.length; i++) {
      formData.append('attachment', payload.files[i]);
      formData.append('filename', payload.filesName[i]);
      formData.append('category', typeof payload.category === 'string' ? payload.category : payload.category[i]);
      formData.append(
        'subcategory',
        typeof payload.subcategory === 'string' ? payload.subcategory : payload.subcategory[i]
      );
      formData.append('validated', payload.validated);
    }
    return this.post(`/interveners-api/interveners/${payload.uuid}/documents`, formData, config);
  };

  getFile = (uuid: string, uuidDocument: string) => {
    return this.get(`/interveners-api/interveners/${uuid}/documents/${uuidDocument}`, { responseType: 'blob' });
  };

  getAccountHistorical = (account_uuid: string): Promise<AxiosResponse> => {
    return this.get(`/accounts-api/accounts/${account_uuid}/historical`);
  }

  generateDocuments = (documentsData: any[]): Promise<AxiosResponse>[] => {
    const promises = [];
    for (const documentData of documentsData) {
      promises.push(this.post("/accounts-api/accounts/registrations", documentData));
    }
    return promises;
  }

  downloadAccountDocument = (account_uuid: string, document_uuid: string): Promise<any> => {
    return this.get(`/accounts-api/accounts/${account_uuid}/documents/${document_uuid}`);
  }

  downloadOperationDocument = (operation_uuid: string, document_uuid: string): Promise<any> => {
    return this.get(`/accounts-api/operations/${operation_uuid}/documents/${document_uuid}`);
  }

  downloadIntervenerDocument = (intervener_uuid: string, document_uuid: string): Promise<any> => {
    return this.get(`/interveners-api/interveners/${intervener_uuid}/documents/${document_uuid}`);
  }
}

export default new OnBoardingService();
