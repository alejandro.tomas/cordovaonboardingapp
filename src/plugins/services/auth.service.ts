import axios, { AxiosRequestConfig, AxiosResponse, AxiosError } from 'axios';

interface AccessTokenData {
    access_token: string;
    expires_in: number;
    refresh_token: string;
    scope: string;
    token_type: string;

}

const Basic = {
    consumer_key: process.env.VUE_APP_CONSUMER_KEY,
    consumer_secret: process.env.VUE_APP_CONSUMER_SECRET
};

/**
 * @override
 */
function generateBasicAuthorization() {
    const token = btoa(`${Basic.consumer_key}:${Basic.consumer_secret}`);
    return `Basic ${token}`;
}

const CONFIG = {
    baseURL: process.env.VUE_APP_API_URL,
    withCredentials: false,
    headers: {
        Accept: '*/*',
        'Content-Type': 'application/x-www-form-urlencoded',
        Authorization: `${generateBasicAuthorization()}`
    }
};

const getAccessToken = (): Promise<AxiosResponse> => {
    const params = new URLSearchParams();
    params.append('grant_type', process.env.VUE_APP_AUTHENTICATION_TYPE);
    if (process.env.VUE_APP_AUTHENTICATION_TYPE === 'password') {
        params.append('username', process.env.VUE_APP_AUTHENTICATION_USERNAME);
        params.append('password', process.env.VUE_APP_AUTHENTICATION_PASSWORD)
    }
    return axios.post('/token', params, CONFIG);
};

const saveAuthData = (data: AccessTokenData) => {
    return new Promise(resolve => {
        localStorage.setItem('COAT', btoa(data.access_token));
        const expires_in = Date.now() + data.expires_in * 1000;
        localStorage.setItem('COEI', btoa(String(expires_in)));
        localStorage.setItem('CORT', btoa(data.refresh_token));
        localStorage.setItem('COSC', btoa(data.scope));
        localStorage.setItem('COTT', btoa(data.token_type));
        resolve({
            ...data,
            ...{ expires_in }
        });
    });
};

const isAuthorized = () => {
    const now = Date.now();
    const accessToken = localStorage.getItem('COAT');
    const expiresIn = localStorage.getItem('COEI');

    if (!accessToken || !expiresIn) {
        return false;
    }
    else {
        const dExpiresIn = atob(expiresIn);
        if (+dExpiresIn < now) {
            return false;
        }
    }
    return true;
};

const authenticate = () => {
    if (isAuthorized()) {
        return Promise.resolve();
    }
    return getAccessToken().then(response => saveAuthData(response.data));
};

const authorizationInterceptor = async (config: AxiosRequestConfig) => {
    try {
        if (!isAuthorized())
            await getAccessToken().then(response => saveAuthData(response.data));
        const prefix = atob(localStorage.getItem('COTT') || '');
        const token = atob(localStorage.getItem('COAT') || '');
        // const expiration_date = atob(localStorage.getItem('COEI') || '');
        // console.log(`${prefix} ${token} - ${new Date(parseInt(expiration_date))}`);
        config.headers.Authorization = `${prefix} ${token}`;
    } catch (err) {
        console.warn(err);
    }
    return config;
};

const authorizationInterceptorOnError = (err: AxiosError) => {
    return Promise.reject(err);
};

export default {
    authenticate,
    authorizationInterceptor,
    authorizationInterceptorOnError,
    isAuthorized,
    getAccessToken,
    saveAuthData
};
