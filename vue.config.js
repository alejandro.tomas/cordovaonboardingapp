module.exports = {
  devServer: {
    https: false,
    // port: 9000,
    disableHostCheck: true,
  },
  publicPath: "./",
  pages: {
    index: {
      entry: "src/index.ts",
    },
  },
  outputDir: "./www/",
};
